//go:build js && wasm

// Package event contains constructors of DOM Events.
// Generates with texts from https://developer.mozilla.org/en-US/docs/Web/Events page
package event

import (
	"gitlab.com/mogowk/mogowk"
)

//
// Specification: Clipboard
//   It provides operations for overriding the default clipboard actions (cut, copy
//   and paste), and for directly accessing the clipboard contents
//
// https://www.w3.org/TR/clipboard-apis/#copy-event
//

// Copy is a "copy" Event
//   ClipboardEvent - The text selection has been added to the clipboard
//
// https://developer.mozilla.org/en-US/docs/Web/Events/copy
func Copy(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "copy", Handler: handler}
}

// Cut is a "cut" Event
//   ClipboardEvent - The text selection has been removed from the document and
//   added to the clipboard
//
// https://developer.mozilla.org/en-US/docs/Web/Events/cut
func Cut(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "cut", Handler: handler}
}

// Paste is a "paste" Event
//   ClipboardEvent - Data has been transferred from the system clipboard to the
//   document
//
// https://developer.mozilla.org/en-US/docs/Web/Events/paste
func Paste(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "paste", Handler: handler}
}

//
// Specification: CSS Animations
//   The behavior of these keyframe animations can be controlled by specifying their
//   duration, number of repeats, and repeating behavior
//
// https://www.w3.org/TR/css-animations-1/#animation-events
//

// AnimationCancel is a "animationcancel" Event
//   AnimationEvent - A CSS animation has aborted
//
// https://developer.mozilla.org/en-US/docs/Web/Events/animationcancel
func AnimationCancel(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "animationcancel", Handler: handler}
}

// AnimationEnd is a "animationend" Event
//   AnimationEvent - A CSS animation has completed
//
// https://developer.mozilla.org/en-US/docs/Web/Events/animationend
func AnimationEnd(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "animationend", Handler: handler}
}

// AnimationIteration is a "animationiteration" Event
//   AnimationEvent - A CSS animation is repeated
//
// https://developer.mozilla.org/en-US/docs/Web/Events/animationiteration
func AnimationIteration(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "animationiteration", Handler: handler}
}

// AnimationStart is a "animationstart" Event
//   AnimationEvent - A CSS animation has started
//
// https://developer.mozilla.org/en-US/docs/Web/Events/animationstart
func AnimationStart(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "animationstart", Handler: handler}
}

// TransitionEnd is a "transitionend" Event
//   TransitionEvent - A CSS transition has completed.
//
// https://developer.mozilla.org/en-US/docs/Web/Events/transitionend
func TransitionEnd(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "transitionend", Handler: handler}
}

//
// Specification: Device Orientation Events
//   Defines several new DOM events that provide information about the physical
//   orientation and motion of a hosting device
//
// https://w3c.github.io/deviceorientation/spec-source-orientation.html
//

// DeviceMotion is a "devicemotion" Event
//   DeviceMotionEvent - Fresh data is available from a motion sensor
//
// https://developer.mozilla.org/en-US/docs/Web/Events/devicemotion
func DeviceMotion(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "devicemotion", Handler: handler}
}

// DeviceOrientation is a "deviceorientation" Event
//   DeviceOrientationEvent - Fresh data is available from an orientation sensor
//
// https://developer.mozilla.org/en-US/docs/Web/Events/deviceorientation
func DeviceOrientation(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "deviceorientation", Handler: handler}
}

//
// Specification: DOM
//   The definition of '"Mutation observers" and slotchange event' in that
//   specification
//
// https://dom.spec.whatwg.org/
//

// SlotChange is a "slotchange" Event
//   The node contents of a HTMLSlotElement (<slot>) have changed.
//
// https://developer.mozilla.org/en-US/docs/Web/Events/slotchange
func SlotChange(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "slotchange", Handler: handler}
}

//
// Specification: DOM L2
//   DOM Level 2 Event Model ... more in link
//
// https://www.w3.org/TR/DOM-Level-2-Events/events.html
//

// Change is a "change" Event
//   Event - The change event is fired for <input>, <select>, and <textarea>
//   elements when a change to the element's value is committed by the user
//
// https://developer.mozilla.org/en-US/docs/Web/Events/change
func Change(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "change", Handler: handler}
}

// Reset is a "reset" Event
//   Event - A form is reset
//
// https://developer.mozilla.org/en-US/docs/Web/Events/reset
func Reset(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "reset", Handler: handler}
}

// Submit is a "submit" Event
//   Event - A form is submitted
//
// https://developer.mozilla.org/en-US/docs/Web/Events/submit
func Submit(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "submit", Handler: handler}
}

//
// Specification: DOM L3
//   Defines UI Events which extend the DOM Event objects defined in DOM. UI Events
//   are those typically implemented by visual user agents for handling user
//   interaction such as mouse and keyboard input.
//
// https://www.w3.org/TR/DOM-Level-3-Events/
//

// CompositionEnd is a "compositionend" Event
//   CompositionEvent - The composition of a passage of text has been completed or
//   canceled
//
// https://developer.mozilla.org/en-US/docs/Web/Events/compositionend
func CompositionEnd(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "compositionend", Handler: handler}
}

// CompositionStart is a "compositionstart" Event
//   CompositionEvent - The composition of a passage of text is prepared (similar to
//   keydown for a keyboard input, but works with other inputs such as speech
//   recognition)
//
// https://developer.mozilla.org/en-US/docs/Web/Events/compositionstart
func CompositionStart(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "compositionstart", Handler: handler}
}

// CompositionUpdate is a "compositionupdate" Event
//   CompositionEvent - A character is added to a passage of text being composed
//
// https://developer.mozilla.org/en-US/docs/Web/Events/compositionupdate
func CompositionUpdate(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "compositionupdate", Handler: handler}
}

// Blur is a "blur" Event
//   FocusEvent - An element has lost focus (does not bubble)
//
// https://developer.mozilla.org/en-US/docs/Web/Events/blur
func Blur(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "blur", Handler: handler}
}

// Focus is a "focus" Event
//   FocusEvent - An element has received focus (does not bubble)
//
// https://developer.mozilla.org/en-US/docs/Web/Events/focus
func Focus(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "focus", Handler: handler}
}

// FocusIn is a "focusin" Event
//   FocusEvent - An element is about to receive focus (bubbles)
//
// https://developer.mozilla.org/en-US/docs/Web/Events/focusin
func FocusIn(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "focusin", Handler: handler}
}

// FocusOut is a "focusout" Event
//   FocusEvent - An element is about to lose focus (bubbles)
//
// https://developer.mozilla.org/en-US/docs/Web/Events/focusout
func FocusOut(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "focusout", Handler: handler}
}

// KeyDown is a "keydown" Event
//   KeyboardEvent - A key is pressed down
//
// https://developer.mozilla.org/en-US/docs/Web/Events/keydown
func KeyDown(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "keydown", Handler: handler}
}

// KeyUp is a "keyup" Event
//   KeyboardEvent - A key is released
//
// https://developer.mozilla.org/en-US/docs/Web/Events/keyup
func KeyUp(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "keyup", Handler: handler}
}

// Click is a "click" Event
//   MouseEvent - A pointing device button has been pressed and released on an
//   element
//
// https://developer.mozilla.org/en-US/docs/Web/Events/click
func Click(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "click", Handler: handler}
}

// DoubleClick is a "dblclick" Event
//   MouseEvent - A pointing device button is clicked twice on an element
//
// https://developer.mozilla.org/en-US/docs/Web/Events/dblclick
func DoubleClick(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "dblclick", Handler: handler}
}

// MouseDown is a "mousedown" Event
//   MouseEvent - A pointing device button (usually a mouse) is pressed on an
//   element
//
// https://developer.mozilla.org/en-US/docs/Web/Events/mousedown
func MouseDown(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "mousedown", Handler: handler}
}

// MouseEnter is a "mouseenter" Event
//   MouseEvent - A pointing device is moved onto the element that has the listener
//   attached
//
// https://developer.mozilla.org/en-US/docs/Web/Events/mouseenter
func MouseEnter(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "mouseenter", Handler: handler}
}

// MouseLeave is a "mouseleave" Event
//   MouseEvent - A pointing device is moved off the element that has the listener
//   attached
//
// https://developer.mozilla.org/en-US/docs/Web/Events/mouseleave
func MouseLeave(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "mouseleave", Handler: handler}
}

// MouseMove is a "mousemove" Event
//   MouseEvent - A pointing device is moved over an element
//
// https://developer.mozilla.org/en-US/docs/Web/Events/mousemove
func MouseMove(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "mousemove", Handler: handler}
}

// MouseOut is a "mouseout" Event
//   MouseEvent - A pointing device is moved off the element that has the listener
//   attached or off one of its children
//
// https://developer.mozilla.org/en-US/docs/Web/Events/mouseout
func MouseOut(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "mouseout", Handler: handler}
}

// MouseOver is a "mouseover" Event
//   MouseEvent - A pointing device is moved onto the element that has the listener
//   attached or onto one of its children
//
// https://developer.mozilla.org/en-US/docs/Web/Events/mouseover
func MouseOver(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "mouseover", Handler: handler}
}

// MouseUp is a "mouseup" Event
//   MouseEvent - A pointing device button is released over an element
//
// https://developer.mozilla.org/en-US/docs/Web/Events/mouseup
func MouseUp(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "mouseup", Handler: handler}
}

// Abort is a "abort" Event
//   UIEvent - The loading of a resource has been aborted
//   Event (IndexedDB) - A transaction has been aborted
//   ProgressEvent (XMLHttpRequest) - Progression has been terminated (not due to an
//   error)
//
// https://developer.mozilla.org/en-US/docs/Web/Events/abort
// https://developer.mozilla.org/en-US/docs/Web/Reference/Events/abort_indexedDB
// https://developer.mozilla.org/en-US/docs/Web/Reference/Events/abort_(ProgressEvent)
func Abort(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "abort", Handler: handler}
}

// Error is a "error" Event
//   UIEvent - A resource failed to load
//   Event (IndexedDB) - A request caused an error and failed
//   Event (Server Sent Events) - An event source connection has been failed
//   Event (Web Speech API) - A speech recognition error occurs
//   Event (SpeechSynthesisErrorEvent) - An error occurs that prevents the utterance
//   from being successfully spoken
//   Event (WebSocket) - A WebSocket connection has been closed with prejudice
//   ProgressEvent (XMLHttpRequest) - Progression has failed
//
// https://developer.mozilla.org/en-US/docs/Web/Events/error
// https://developer.mozilla.org/en-US/docs/Web/Events/error_(SpeechRecognitionError)
// https://developer.mozilla.org/en-US/docs/Web/Events/error_(SpeechSynthesisError)
func Error(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "error", Handler: handler}
}

// Load is a "load" Event
//   UIEvent - A resource and its dependent resources have finished loading
//   ProgressEvent (XMLHttpRequest) - Progression has been successful
//
// https://developer.mozilla.org/en-US/docs/Web/Events/load
// https://developer.mozilla.org/en-US/docs/Web/Reference/Events/load_(ProgressEvent)
func Load(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "load", Handler: handler}
}

// Resize is a "resize" Event
//   UIEvent - The document view has been resized
//
// https://developer.mozilla.org/en-US/docs/Web/Events/resize
func Resize(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "resize", Handler: handler}
}

// Scroll is a "scroll" Event
//   UIEvent - The document view or an element has been scrolled
//
// https://developer.mozilla.org/en-US/docs/Web/Events/scroll
func Scroll(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "scroll", Handler: handler}
}

// Select is a "select" Event
//   UIEvent - Some text is being selected
//
// https://developer.mozilla.org/en-US/docs/Web/Events/select
func Select(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "select", Handler: handler}
}

// Unload is a "unload" Event
//   UIEvent - The document or a dependent resource is being unloaded
//
// https://developer.mozilla.org/en-US/docs/Web/Events/unload
func Unload(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "unload", Handler: handler}
}

// Wheel is a "wheel" Event
//   WheelEvent - A wheel button of a pointing device is rotated in any direction
//
// https://developer.mozilla.org/en-US/docs/Web/Events/wheel
func Wheel(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "wheel", Handler: handler}
}

//
// Specification: Full Screen
//   Defines the fullscreen API for the web platform
//
// https://www.w3.org/TR/fullscreen/
//

// FullScreenChange is a "fullscreenchange" Event
//   Event - An element was toggled to or from fullscreen mode
//
// https://developer.mozilla.org/en-US/docs/Web/Events/fullscreenchange
func FullScreenChange(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "fullscreenchange", Handler: handler}
}

// FullScreenError is a "fullscreenerror" Event
//   Event - It was impossible to switch to fullscreen mode for technical reasons or
//   because the permission was denied
//
// https://developer.mozilla.org/en-US/docs/Web/Events/fullscreenerror
func FullScreenError(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "fullscreenerror", Handler: handler}
}

//
// Specification: Gamepad
//   Defines a low-level interface that represents gamepad devices
//
// https://www.w3.org/TR/gamepad/
//

// GamepadConnected is a "gamepadconnected" Event
//   GamepadEvent - A gamepad has been connected
//
// https://developer.mozilla.org/en-US/docs/Web/Events/gamepadconnected
func GamepadConnected(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "gamepadconnected", Handler: handler}
}

// GamepadDisconnected is a "gamepaddisconnected" Event
//   GamepadEvent - A gamepad has been disconnected
//
// https://developer.mozilla.org/en-US/docs/Web/Events/gamepaddisconnected
func GamepadDisconnected(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "gamepaddisconnected", Handler: handler}
}

//
// Specification: HTML 5.1
//   Defines the 2nd edition of the 5th major version, first minor revision of the
//   core language (HTML)
//
// https://www.w3.org/TR/html51/#dom-navigator-languages
//

// LanguageChange is a "languagechange" Event
//   The user's preferred languages have changed
//
// https://developer.mozilla.org/en-US/docs/Web/Events/languagechange
func LanguageChange(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "languagechange", Handler: handler}
}

//
// Specification: HTML5
//   Defines the 5th major revision of the core language (HTML)
//
// https://www.w3.org/TR/html50/
//

// BeforeUnload is a "beforeunload" Event
//   BeforeUnloadEvent - The window, the document and its resources are about to be
//   unloaded
//
// https://developer.mozilla.org/en-US/docs/Web/Events/beforeunload
func BeforeUnload(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "beforeunload", Handler: handler}
}

// Drag is a "drag" Event
//   DragEvent - An element or text selection is being dragged (every 350ms)
//
// https://developer.mozilla.org/en-US/docs/Web/Events/drag
func Drag(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "drag", Handler: handler}
}

// DragEnd is a "dragend" Event
//   DragEvent - A drag operation is being ended (by releasing a mouse button or
//   hitting the escape key)
//
// https://developer.mozilla.org/en-US/docs/Web/Events/dragend
func DragEnd(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "dragend", Handler: handler}
}

// DragEnter is a "dragenter" Event
//   DragEvent - A dragged element or text selection enters a valid drop target
//
// https://developer.mozilla.org/en-US/docs/Web/Events/dragenter
func DragEnter(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "dragenter", Handler: handler}
}

// DragLeave is a "dragleave" Event
//   DragEvent - A dragged element or text selection leaves a valid drop target
//
// https://developer.mozilla.org/en-US/docs/Web/Events/dragleave
func DragLeave(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "dragleave", Handler: handler}
}

// DragOver is a "dragover" Event
//   DragEvent - An element or text selection is being dragged over a valid drop
//   target
//   (fires every 350ms)
//
// https://developer.mozilla.org/en-US/docs/Web/Events/dragover
func DragOver(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "dragover", Handler: handler}
}

// DragStart is a "dragstart" Event
//   DragEvent - The user starts dragging an element or text selection
//
// https://developer.mozilla.org/en-US/docs/Web/Events/dragstart
func DragStart(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "dragstart", Handler: handler}
}

// Drop is a "drop" Event
//   DragEvent - An element is dropped on a valid drop target
//
// https://developer.mozilla.org/en-US/docs/Web/Events/drop
func Drop(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "drop", Handler: handler}
}

// AfterPrint is a "afterprint" Event
//   Event - The associated document has started printing or the print preview has
//   been closed
//
// https://developer.mozilla.org/en-US/docs/Web/Events/afterprint
func AfterPrint(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "afterprint", Handler: handler}
}

// BeforePrint is a "beforeprint" Event
//   Event - The associated document is about to be printed or previewed for
//   printing
//
// https://developer.mozilla.org/en-US/docs/Web/Events/beforeprint
func BeforePrint(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "beforeprint", Handler: handler}
}

// DOMContentLoaded is a "DOMContentLoaded" Event
//   Event - The document has finished loading (but not its dependent resources)
//
// https://developer.mozilla.org/en-US/docs/Web/Events/DOMContentLoaded
func DOMContentLoaded(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "DOMContentLoaded", Handler: handler}
}

// Input is a "input" Event
//   Event - The value of an element changes or the content of an element with the
//   attribute contenteditable is modified
//
// https://developer.mozilla.org/en-US/docs/Web/Events/input
func Input(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "input", Handler: handler}
}

// Invalid is a "invalid" Event
//   Event - A submittable element has been checked and doesn't satisfy its
//   constraints
//
// https://developer.mozilla.org/en-US/docs/Web/Events/invalid
func Invalid(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "invalid", Handler: handler}
}

// HashChange is a "hashchange" Event
//   HashChangeEvent - The fragment identifier of the URL has changed (the part of
//   the URL after the #)
//
// https://developer.mozilla.org/en-US/docs/Web/Events/hashchange
func HashChange(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "hashchange", Handler: handler}
}

// ContextMenu is a "contextmenu" Event
//   MouseEvent - The right button of the mouse is clicked (before the context menu
//   is displayed)
//
// https://developer.mozilla.org/en-US/docs/Web/Events/contextmenu
func ContextMenu(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "contextmenu", Handler: handler}
}

// Show is a "show" Event
//   MouseEvent - A contextmenu event was fired on (or bubbled to) an element that
//   has a contextmenu attribute
//
// https://developer.mozilla.org/en-US/docs/Web/Events/show
func Show(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "show", Handler: handler}
}

// PageHide is a "pagehide" Event
//   PageTransitionEvent - A session history entry is being traversed from
//
// https://developer.mozilla.org/en-US/docs/Web/Events/pagehide
func PageHide(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "pagehide", Handler: handler}
}

// PageShow is a "pageshow" Event
//   PageTransitionEvent - A session history entry is being traversed to
//
// https://developer.mozilla.org/en-US/docs/Web/Events/pageshow
func PageShow(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "pageshow", Handler: handler}
}

// PopState is a "popstate" Event
//   PopStateEvent - Fired when the active history entry changes while the user
//   navigates the session history
//
// https://developer.mozilla.org/en-US/docs/Web/Events/popstate
func PopState(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "popstate", Handler: handler}
}

// ReadyStateChange is a "readystatechange" Event
//   Event - The readyState attribute of a document has changed
//   Event (XMLHttpRequest) - The readyState attribute of a document has changed
//
// https://developer.mozilla.org/en-US/docs/Web/Events/readystatechange
// https://developer.mozilla.org/en-US/docs/Web/Events/readystatechange
func ReadyStateChange(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "readystatechange", Handler: handler}
}

//
// Specification: HTML5 media
//   Used to present audio data, or video and audio data, to the user. This is
//   referred to as media data in this section, since this section applies equally to
//   media elements for audio or for video
//
// https://www.w3.org/TR/html50/embedded-content-0.html#media-element
//

// CanPlay is a "canplay" Event
//   Event - The user agent can play the media, but estimates that not enough data
//   has been loaded to play the media up to its end without having to stop for
//   further buffering of content
//
// https://developer.mozilla.org/en-US/docs/Web/Events/canplay
func CanPlay(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "canplay", Handler: handler}
}

// CanPlayThrough is a "canplaythrough" Event
//   Event - The user agent can play the media up to its end without having to stop
//   for further buffering of content
//
// https://developer.mozilla.org/en-US/docs/Web/Events/canplaythrough
func CanPlayThrough(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "canplaythrough", Handler: handler}
}

// DurationChange is a "durationchange" Event
//   Event - The duration attribute has been updated
//
// https://developer.mozilla.org/en-US/docs/Web/Events/durationchange
func DurationChange(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "durationchange", Handler: handler}
}

// Emptied is a "emptied" Event
//   Event - The media has become empty For example, this event is triggered if the
//   media has already been loaded (or partially loaded), and the load() method is
//   called to reload it
//
// https://developer.mozilla.org/en-US/docs/Web/Events/emptied
func Emptied(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "emptied", Handler: handler}
}

// Ended is a "ended" Event
//   Event - Playback has stopped because the end of the media was reached
//   Event - (Web Audio API) Playback has stopped because the end of the media was
//   reached
//
// https://developer.mozilla.org/en-US/docs/Web/Events/ended
// https://developer.mozilla.org/en-US/docs/Web/Events/ended_(Web_Audio)
func Ended(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "ended", Handler: handler}
}

// LoadedData is a "loadeddata" Event
//   Event - The first frame of the media has finished loading
//
// https://developer.mozilla.org/en-US/docs/Web/Events/loadeddata
func LoadedData(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "loadeddata", Handler: handler}
}

// LoadedMetaData is a "loadedmetadata" Event
//   Event - The metadata has been loaded
//
// https://developer.mozilla.org/en-US/docs/Web/Events/loadedmetadata
func LoadedMetaData(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "loadedmetadata", Handler: handler}
}

// Pause is a "pause" Event
//   Event - Playback has been paused
//   SpeechSynthesisEvent (Web Speech API) - The utterance is paused part way
//   through
//
// https://developer.mozilla.org/en-US/docs/Web/Events/pause
// https://developer.mozilla.org/en-US/docs/Web/Events/pause_(SpeechSynthesis)
func Pause(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "pause", Handler: handler}
}

// Play is a "play" Event
//   Event - Playback has begun
//
// https://developer.mozilla.org/en-US/docs/Web/Events/play
func Play(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "play", Handler: handler}
}

// Playing is a "playing" Event
//   Event - Playback is ready to start after having been paused or delayed due to
//   lack of data
//
// https://developer.mozilla.org/en-US/docs/Web/Events/playing
func Playing(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "playing", Handler: handler}
}

// RateChange is a "ratechange" Event
//   Event - The playback rate has changed
//
// https://developer.mozilla.org/en-US/docs/Web/Events/ratechange
func RateChange(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "ratechange", Handler: handler}
}

// Seeked is a "seeked" Event
//   Event - A seek operation completed
//
// https://developer.mozilla.org/en-US/docs/Web/Events/seeked
func Seeked(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "seeked", Handler: handler}
}

// Seeking is a "seeking" Event
//   Event - A seek operation began
//
// https://developer.mozilla.org/en-US/docs/Web/Events/seeking
func Seeking(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "seeking", Handler: handler}
}

// Stalled is a "stalled" Event
//   Event - The user agent is trying to fetch media data, but data is unexpectedly
//   not forthcoming
//
// https://developer.mozilla.org/en-US/docs/Web/Events/stalled
func Stalled(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "stalled", Handler: handler}
}

// Suspend is a "suspend" Event
//   Event - Media data loading has been suspended
//
// https://developer.mozilla.org/en-US/docs/Web/Events/suspend
func Suspend(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "suspend", Handler: handler}
}

// TimeUpdate is a "timeupdate" Event
//   Event - The time indicated by the currentTime attribute has been updated
//
// https://developer.mozilla.org/en-US/docs/Web/Events/timeupdate
func TimeUpdate(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "timeupdate", Handler: handler}
}

// VolumeChange is a "volumechange" Event
//   Event - The volume has changed
//
// https://developer.mozilla.org/en-US/docs/Web/Events/volumechange
func VolumeChange(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "volumechange", Handler: handler}
}

// Waiting is a "waiting" Event
//   Event - Playback has stopped because of a temporary lack of data
//
// https://developer.mozilla.org/en-US/docs/Web/Events/waiting
func Waiting(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "waiting", Handler: handler}
}

// Offline is a "offline" Event
//   Event - The browser has lost access to the network
//
// https://developer.mozilla.org/en-US/docs/Web/Events/offline
func Offline(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "offline", Handler: handler}
}

// Online is a "online" Event
//   Event - The browser has gained access to the network (but particular websites
//   might be unreachable)
//
// https://developer.mozilla.org/en-US/docs/Web/Events/online
func Online(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "online", Handler: handler}
}

//
// Specification: IndexedDB
//   Defines APIs for a database of records holding simple values and hierarchical
//   objects. Each record consists of a key and some value
//
// https://www.w3.org/TR/IndexedDB/
//

// "abort" Event (duplicate name)
//   Event - A transaction has been aborted
//
// https://developer.mozilla.org/en-US/docs/Web/Reference/Events/abort_indexedDB

// "error" Event (duplicate name)
//   Event - A request caused an error and failed
//
// https://developer.mozilla.org/en-US/docs/Web/Events/error

// Success is a "success" Event
//   Event - A request successfully completed
//
// https://developer.mozilla.org/en-US/docs/Web/Reference/Events/success_indexedDB
func Success(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "success", Handler: handler}
}

// Blocked is a "blocked" Event
//   Event - An open connection to a database is blocking a versionchange
//   transaction on the same database
//
// https://developer.mozilla.org/en-US/docs/Web/Reference/Events/blocked_indexedDB
func Blocked(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "blocked", Handler: handler}
}

// Complete is a "complete" Event
//   Event - A transaction successfully completed
//   OfflineAudioCompletionEvent (Web Audio API) - The rendering of an
//   OfflineAudioContext is terminated",
//
// https://developer.mozilla.org/en-US/docs/Web/Reference/Events/complete_indexedDB
// https://developer.mozilla.org/en-US/docs/Web/Events/complete
func Complete(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "complete", Handler: handler}
}

// UpgradeNeeded is a "upgradeneeded" Event
//   Event - An attempt was made to open a database with a version number higher
//   than its current version A versionchange transaction has been created
//
// https://developer.mozilla.org/en-US/docs/Web/Reference/Events/upgradeneeded_indexedDB
func UpgradeNeeded(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "upgradeneeded", Handler: handler}
}

// VersionChange is a "versionchange" Event
//   Event - A versionchange transaction completed
//
// https://developer.mozilla.org/en-US/docs/Web/Reference/Events/versionchange_indexedDB
func VersionChange(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "versionchange", Handler: handler}
}

//
// Specification: Media Capture and Streams
//   Defines a set of JavaScript APIs that allow local media, including audio and
//   video, to be requested from a platform
//
// https://w3c.github.io/mediacapture-main/
//

// DeviceChange is a "devicechange" Event
//   Event - A media device such as a camera, microphone, or speaker is connected or
//   removed from the system
//
// https://developer.mozilla.org/en-US/docs/Web/Events/devicechange
func DeviceChange(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "devicechange", Handler: handler}
}

//
// Specification: Notifications API
//   Defines an API for end-user notifications. A notification allows alerting the
//   user outside the context of a web page of an occurrence, such as the delivery of
//   email
//
// https://www.w3.org/TR/notifications/
//

// NotificationClick is a "notificationclick" Event
//   NotificationEvent - A system notification spawned by
//   ServiceWorkerRegistrationshowNotification() has been clicked
//
// https://developer.mozilla.org/en-US/docs/Web/Events/notificationclick
func NotificationClick(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "notificationclick", Handler: handler}
}

//
// Specification: Page visibility
//   Defines a means to programmatically determine the visibility state of a
//   document. This can aid in the development of resource efficient web applications
//
// http://www.w3.org/TR/page-visibility
//

// VisibilityChange is a "visibilitychange" Event
//   Event - The content of a tab has become visible or has been hidden
//
// https://developer.mozilla.org/en-US/docs/Web/Events/visibilitychange
func VisibilityChange(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "visibilitychange", Handler: handler}
}

//
// Specification: Pointer Events
//   Describes events and related interfaces for handling hardware agnostic pointer
//   input from devices including a mouse, pen, touchscreen, etc
//
// https://www.w3.org/TR/pointerevents/#the-gotpointercapture-event
//

// GotPointerCapture is a "gotpointercapture" Event
//   PointerEvent - Element receives pointer capture
//
// https://developer.mozilla.org/en-US/docs/Web/Events/gotpointercapture
func GotPointerCapture(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "gotpointercapture", Handler: handler}
}

// LostPointerCapture is a "lostpointercapture" Event
//   PointerEvent - Element lost pointer capture
//
// https://developer.mozilla.org/en-US/docs/Web/Events/lostpointercapture
func LostPointerCapture(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "lostpointercapture", Handler: handler}
}

// PointerCancel is a "pointercancel" Event
//   PointerEvent - The pointer is unlikely to produce any more events
//
// https://developer.mozilla.org/en-US/docs/Web/Events/pointercancel
func PointerCancel(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "pointercancel", Handler: handler}
}

// PointerDown is a "pointerdown" Event
//   PointerEvent - The pointer enters the active buttons state
//
// https://developer.mozilla.org/en-US/docs/Web/Events/pointerdown
func PointerDown(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "pointerdown", Handler: handler}
}

// PointerEnter is a "pointerenter" Event
//   PointerEvent - Pointing device is moved inside the hit-testing boundary
//
// https://developer.mozilla.org/en-US/docs/Web/Events/pointerenter
func PointerEnter(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "pointerenter", Handler: handler}
}

// PointerLeave is a "pointerleave" Event
//   PointerEvent - Pointing device is moved out of the hit-testing boundary
//
// https://developer.mozilla.org/en-US/docs/Web/Events/pointerleave
func PointerLeave(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "pointerleave", Handler: handler}
}

// PointerMove is a "pointermove" Event
//   PointerEvent - The pointer changed coordinates
//
// https://developer.mozilla.org/en-US/docs/Web/Events/pointermove
func PointerMove(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "pointermove", Handler: handler}
}

// PointerOut is a "pointerout" Event
//   PointerEvent - The pointing device moved out of hit-testing boundary or leaves
//   detectable hover range
//
// https://developer.mozilla.org/en-US/docs/Web/Events/pointerout
func PointerOut(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "pointerout", Handler: handler}
}

// PointerOver is a "pointerover" Event
//   PointerEvent - The pointing device is moved into the hit-testing boundary
//
// https://developer.mozilla.org/en-US/docs/Web/Events/pointerover
func PointerOver(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "pointerover", Handler: handler}
}

// PointerUp is a "pointerup" Event
//   PointerEvent - The pointer leaves the active buttons state
//
// https://developer.mozilla.org/en-US/docs/Web/Events/pointerup
func PointerUp(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "pointerup", Handler: handler}
}

//
// Specification: Pointer Lock
//   Defines an API that provides scripted access to raw mouse movement data while
//   locking the target of mouse events to a single element and removing the cursor
//   from view
//
// https://www.w3.org/TR/pointerlock/
//

// PointerLockChange is a "pointerlockchange" Event
//   Event - The pointer was locked or released
//
// https://developer.mozilla.org/en-US/docs/Web/Events/pointerlockchange
func PointerLockChange(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "pointerlockchange", Handler: handler}
}

// PointerLockError is a "pointerlockerror" Event
//   Event - It was impossible to lock the pointer for technical reasons or because
//   the permission was denied
//
// https://developer.mozilla.org/en-US/docs/Web/Events/pointerlockerror
func PointerLockError(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "pointerlockerror", Handler: handler}
}

//
// Specification: Push API
//   Enables sending of a push message to a web application via a push service
//
// https://w3c.github.io/push-api/
//

// Push is a "push" Event
//   PushEvent - A Service Worker has received a push message
//
// https://developer.mozilla.org/en-US/docs/Web/Events/push
func Push(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "push", Handler: handler}
}

// PushSubscriptionChange is a "pushsubscriptionchange" Event
//   PushEvent - A PushSubscription has expired
//
// https://developer.mozilla.org/en-US/docs/Web/Events/pushsubscriptionchange
func PushSubscriptionChange(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "pushsubscriptionchange", Handler: handler}
}

//
// Specification: Resource Timing
//   Defines an interface for web applications to access the complete timing
//   information for resources in a document
//
// https://w3c.github.io/resource-timing/
//

// ResourceTimingBufferFull is a "resourcetimingbufferfull" Event
//   Performance - The browser's resource timing buffer is full
//
// https://developer.mozilla.org/en-US/docs/Web/Events/resourcetimingbufferfull
func ResourceTimingBufferFull(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "resourcetimingbufferfull", Handler: handler}
}

//
// Specification: Screen Orientation
//   Provides the ability to read the screen orientation type and angle, to be
//   informed when the screen orientation changes, and to lock the screen to a
//   specific orientation
//
// https://www.w3.org/TR/screen-orientation/
//

// OrientationChange is a "orientationchange" Event
//   Event - The orientation of the device (portrait/landscape) has changed
//
// https://developer.mozilla.org/en-US/docs/Web/Events/orientationchange
func OrientationChange(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "orientationchange", Handler: handler}
}

//
// Specification: Selection API
//   Defines APIs for selection, which allows users and authors to select a portion
//   of a document or specify a point of interest for copy, paste, and other editing
//   operations
//
// https://w3c.github.io/selection-api/
//

// SelectionChange is a "selectionchange" Event
//   Event - The selection in the document has been changed
//
// https://developer.mozilla.org/en-US/docs/Web/API/Document/selectionchange_event
func SelectionChange(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "selectionchange", Handler: handler}
}

// SelectStart is a "selectstart" Event
//   Event - A selection just started
//
// https://developer.mozilla.org/en-US/docs/Web/API/Document/selectstart_event
func SelectStart(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "selectstart", Handler: handler}
}

//
// Specification: Server Sent Events
//   Defines an API for opening an HTTP connection for receiving push notifications
//   from a server in the form of DOM events
//
// https://www.w3.org/TR/eventsource/
//

// "error" Event (duplicate name)
//   Event - An event source connection has been failed
//
// https://developer.mozilla.org/en-US/docs/Web/Events/error

// Open is a "open" Event
//   Event - An event source connection has been established
//   Event (WebSocket) - A WebSocket connection has been established
//
// https://developer.mozilla.org/en-US/docs/Web/Reference/Events/open_serversentevents
// https://developer.mozilla.org/en-US/docs/Web/Reference/Events/open_websocket
func Open(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "open", Handler: handler}
}

// Message is a "message" Event
//   MessageEvent - A message is received through an event source
//   ServiceWorkerMessageEvent (Service Workers) - A message is received from a
//   service worker, or a message is received in a service worker from another
//   context
//   MessageEvent (Web Workers) - A message is received from a Web Worker
//   MessageEvent (WebSocket) - A message is received through a WebSocket
//
// https://developer.mozilla.org/en-US/docs/Web/Reference/Events/message_serversentevents
// https://developer.mozilla.org/en-US/docs/Web/Events/message_(ServiceWorker)
// https://developer.mozilla.org/en-US/docs/Web/Reference/Events/message_webworker
// https://developer.mozilla.org/en-US/docs/Web/Reference/Events/message_websocket
func Message(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "message", Handler: handler}
}

//
// Specification: Service Workers
//   Service workers essentially act as proxy servers that sit between web
//   applications, the browser, and the network (when available)
//
// https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API
//

// "message" Event (duplicate name)
//   ServiceWorkerMessageEvent - A message is received from a service worker, or a
//   message is received in a service worker from another context
//
// https://developer.mozilla.org/en-US/docs/Web/Events/message_(ServiceWorker)

//
// Specification: SVG
//   Describes information about events, including under which circumstances events
//   are triggered and how to indicate whether a given document can be zoomed and
//   panned
//
// https://www.w3.org/TR/SVG/interact.html
//

// SVGAbort is a "SVGAbort" Event
//   SVGEvent - Page loading has been stopped before the SVG was loaded
//
// https://developer.mozilla.org/en-US/docs/Web/Events/SVGAbort
func SVGAbort(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "SVGAbort", Handler: handler}
}

// SVGError is a "SVGError" Event
//   SVGEvent - An error has occurred before the SVG was loaded
//
// https://developer.mozilla.org/en-US/docs/Web/Events/SVGError
func SVGError(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "SVGError", Handler: handler}
}

// SVGLoad is a "SVGLoad" Event
//   SVGEvent - An SVG document has been loaded and parsed
//
// https://developer.mozilla.org/en-US/docs/Web/Events/SVGLoad
func SVGLoad(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "SVGLoad", Handler: handler}
}

// SVGResize is a "SVGResize" Event
//   SVGEvent - An SVG document is being resized
//
// https://developer.mozilla.org/en-US/docs/Web/Events/SVGResize
func SVGResize(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "SVGResize", Handler: handler}
}

// SVGScroll is a "SVGScroll" Event
//   SVGEvent - An SVG document is being scrolled
//
// https://developer.mozilla.org/en-US/docs/Web/Events/SVGScroll
func SVGScroll(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "SVGScroll", Handler: handler}
}

// SVGUnload is a "SVGUnload" Event
//   SVGEvent - An SVG document has been removed from a window or frame
//
// https://developer.mozilla.org/en-US/docs/Web/Events/SVGUnload
func SVGUnload(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "SVGUnload", Handler: handler}
}

// SVGZoom is a "SVGZoom" Event
//   SVGZoomEvent - An SVG document is being zoomed
//
// https://developer.mozilla.org/en-US/docs/Web/Events/SVGZoom
func SVGZoom(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "SVGZoom", Handler: handler}
}

// BeginEvent is a "beginEvent" Event
//   TimeEvent - A SMIL animation element begins
//
// https://developer.mozilla.org/en-US/docs/Web/Events/beginEvent
func BeginEvent(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "beginEvent", Handler: handler}
}

// EndEvent is a "endEvent" Event
//   TimeEvent - A SMIL animation element ends
//
// https://developer.mozilla.org/en-US/docs/Web/Events/endEvent
func EndEvent(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "endEvent", Handler: handler}
}

// RepeatEvent is a "repeatEvent" Event
//   TimeEvent - A SMIL animation element is repeated
//
// https://developer.mozilla.org/en-US/docs/Web/Events/repeatEvent
func RepeatEvent(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "repeatEvent", Handler: handler}
}

//
// Specification: Touch Events
//   Defines a set of low-level events that represent one or more points of contact
//   with a touch-sensitive surface
//
// https://www.w3.org/TR/touch-events/
//

// TouchCancel is a "touchcancel" Event
//   TouchEvent - A touch point has been disrupted in an implementation-specific
//   manner (too many touch points, for example)
//
// https://developer.mozilla.org/en-US/docs/Web/Events/touchcancel
func TouchCancel(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "touchcancel", Handler: handler}
}

// TouchEnd is a "touchend" Event
//   TouchEvent - A touch point is removed from the touch surface
//
// https://developer.mozilla.org/en-US/docs/Web/Events/touchend
func TouchEnd(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "touchend", Handler: handler}
}

// TouchMove is a "touchmove" Event
//   TouchEvent - A touch point is moved along the touch surface
//
// https://developer.mozilla.org/en-US/docs/Web/Events/touchmove
func TouchMove(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "touchmove", Handler: handler}
}

// TouchStart is a "touchstart" Event
//   TouchEvent - A touch point is placed on the touch surface
//
// https://developer.mozilla.org/en-US/docs/Web/Events/touchstart
func TouchStart(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "touchstart", Handler: handler}
}

//
// Specification: Web Audio API
//   Describes a high-level Web API for processing and synthesizing audio in web
//   applications
//
// https://webaudio.github.io/web-audio-api/
//

// "ended" Event (duplicate name)
//   Event - Playback has stopped because the end of the media was reached
//
// https://developer.mozilla.org/en-US/docs/Web/Events/ended_(Web_Audio)

// "complete" Event (duplicate name)
//   OfflineAudioCompletionEvent - The rendering of an OfflineAudioContext is
//   terminated
//
// https://developer.mozilla.org/en-US/docs/Web/Events/complete

//
// Specification: Web Speech API
//   Defines a JavaScript API to enable web developers to incorporate speech
//   recognition and synthesis into their web pages
//
// https://wicg.github.io/speech-api/
//

// AudioEnd is a "audioend" Event
//   Event - The user agent has finished capturing audio for speech recognition
//
// https://developer.mozilla.org/en-US/docs/Web/Events/audioend
func AudioEnd(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "audioend", Handler: handler}
}

// AudioStart is a "audiostart" Event
//   Event - The user agent has started to capture audio for speech recognition
//
// https://developer.mozilla.org/en-US/docs/Web/Events/audiostart
func AudioStart(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "audiostart", Handler: handler}
}

// End is a "end" Event
//   Event (Web Speech API) - The speech recognition service has disconnected
//   SpeechSynthesisEvent (Web Speech API) - The utterance has finished being spoken
//
// https://developer.mozilla.org/en-US/docs/Web/Events/end_(SpeechRecognition)
// https://developer.mozilla.org/en-US/docs/Web/Events/end_(SpeechSynthesis)
func End(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "end", Handler: handler}
}

// "error" Event (duplicate name)
//   Event - A speech recognition error occurs
//
// https://developer.mozilla.org/en-US/docs/Web/Events/error_(SpeechRecognitionError)

// SoundEnd is a "soundend" Event
//   Event - Any sound — recognizable speech or not — has stopped being detected
//
// https://developer.mozilla.org/en-US/docs/Web/Events/soundend
func SoundEnd(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "soundend", Handler: handler}
}

// SoundStart is a "soundstart" Event
//   Event - Any sound — recognizable speech or not — has been detected
//
// https://developer.mozilla.org/en-US/docs/Web/Events/soundstart
func SoundStart(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "soundstart", Handler: handler}
}

// SpeechEnd is a "speechend" Event
//   Event - Speech recognized by the speech recognition service has stopped being
//   detected
//
// https://developer.mozilla.org/en-US/docs/Web/Events/speechend
func SpeechEnd(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "speechend", Handler: handler}
}

// SpeechStart is a "speechstart" Event
//   Event - Sound that is recognized by the speech recognition service as speech
//   has been detected
//
// https://developer.mozilla.org/en-US/docs/Web/Events/speechstart
func SpeechStart(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "speechstart", Handler: handler}
}

// Start is a "start" Event
//   Event - The speech recognition service has begun listening to incoming audio
//   with intent to recognize grammars associated with the current SpeechRecognition
//   SpeechSynthesisEvent (Web Speech API) - The utterance has begun to be spoken
//
// https://developer.mozilla.org/en-US/docs/Web/Events/start_(SpeechRecognition)
// https://developer.mozilla.org/en-US/docs/Web/Events/start_(SpeechSynthesis)
func Start(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "start", Handler: handler}
}

// VoicesChanged is a "voiceschanged" Event
//   Event - The list of SpeechSynthesisVoice objects that would be returned by the
//   SpeechSynthesisgetVoices() method has changed (when the voiceschanged event
//   fires)
//
// https://developer.mozilla.org/en-US/docs/Web/Events/voiceschanged
func VoicesChanged(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "voiceschanged", Handler: handler}
}

// NoMatch is a "nomatch" Event
//   SpeechRecognitionEvent - The speech recognition service returns a final result
//   with no significant recognition
//
// https://developer.mozilla.org/en-US/docs/Web/Events/nomatch
func NoMatch(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "nomatch", Handler: handler}
}

// Result is a "result" Event
//   SpeechRecognitionEvent - The speech recognition service returns a result — a
//   word or phrase has been positively recognized and this has been communicated
//   back to the app
//
// https://developer.mozilla.org/en-US/docs/Web/Events/result
func Result(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "result", Handler: handler}
}

// "error" Event (duplicate name)
//   SpeechSynthesisErrorEvent - An error occurs that prevents the utterance from
//   being successfully spoken
//
// https://developer.mozilla.org/en-US/docs/Web/Events/error_(SpeechSynthesisError)

// Boundary is a "boundary" Event
//   SpeechSynthesisEvent - The spoken utterance reaches a word or sentence boundary
//
// https://developer.mozilla.org/en-US/docs/Web/Events/boundary
func Boundary(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "boundary", Handler: handler}
}

// "end" Event (duplicate name)
//   SpeechSynthesisEvent - The utterance has finished being spoken
//
// https://developer.mozilla.org/en-US/docs/Web/Events/end_(SpeechSynthesis)

// Mark is a "mark" Event
//   SpeechSynthesisEvent - The spoken utterance reaches a named SSML "mark" tag
//
// https://developer.mozilla.org/en-US/docs/Web/Events/mark
func Mark(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "mark", Handler: handler}
}

// "pause" Event (duplicate name)
//   SpeechSynthesisEvent - The utterance is paused part way through
//
// https://developer.mozilla.org/en-US/docs/Web/Events/pause_(SpeechSynthesis)

// Resume is a "resume" Event
//   SpeechSynthesisEvent - A paused utterance is resumed
//
// https://developer.mozilla.org/en-US/docs/Web/Events/resume
func Resume(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "resume", Handler: handler}
}

// "start" Event (duplicate name)
//   SpeechSynthesisEvent - The utterance has begun to be spoken
//
// https://developer.mozilla.org/en-US/docs/Web/Events/start_(SpeechSynthesis)

//
// Specification: Web Storage
//   Defines an API for persistent data storage of key-value pair data in Web
//   clients
//
// https://www.w3.org/TR/webstorage
//

// Storage is a "storage" Event
//   StorageEvent - A storage area (localStorage or sessionStorage) has changed
//
// https://developer.mozilla.org/en-US/docs/Web/Events/storage
func Storage(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "storage", Handler: handler}
}

//
// Specification: Web Workers
//   Defines an API that allows Web application authors to spawn background workers
//   running scripts in parallel to their main page
//
// https://www.w3.org/TR/workers/
//

// "message" Event (duplicate name)
//   MessageEvent - A message is received from a Web Worker
//
// https://developer.mozilla.org/en-US/docs/Web/Reference/Events/message_webworker

//
// Specification: WebSocket
//   Defines an API that enables Web pages to use the WebSocket protocol (defined by
//   the IETF) for two-way communication with a remote host
//
// https://www.w3.org/TR/websockets/
//

// Close is a "close" Event
//   Event - A WebSocket connection has been closed
//
// https://developer.mozilla.org/en-US/docs/Web/Reference/Events/close_websocket
func Close(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "close", Handler: handler}
}

// "error" Event (duplicate name)
//   Event - A WebSocket connection has been closed with prejudice (some data
//   couldn't be sent for example)
//
// https://developer.mozilla.org/en-US/docs/Web/Events/error

// "open" Event (duplicate name)
//   Event - A WebSocket connection has been established
//
// https://developer.mozilla.org/en-US/docs/Web/Reference/Events/open_websocket

// "message" Event (duplicate name)
//   MessageEvent - A message is received through a WebSocket
//
// https://developer.mozilla.org/en-US/docs/Web/Reference/Events/message_websocket

// MessageError is a "messageerror" Event
//   MessageEvent - A message error is raised when a message is received by an
//   object
//
// https://developer.mozilla.org/en-US/docs/Web/Events/messageerror
func MessageError(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "messageerror", Handler: handler}
}

//
// Specification: XMLHttpRequest
//   Defines an API that provides scripted client functionality for transferring
//   data between a client and a server
//
// https://www.w3.org/TR/XMLHttpRequest/
//

// "abort" Event (duplicate name)
//   ProgressEvent - Progression has been terminated (not due to an error)
//
// https://developer.mozilla.org/en-US/docs/Web/Reference/Events/abort_(ProgressEvent)

// "error" Event (duplicate name)
//   ProgressEvent - Progression has failed
//
// https://developer.mozilla.org/en-US/docs/Web/Events/error

// "load" Event (duplicate name)
//   ProgressEvent - Progression has been successful
//
// https://developer.mozilla.org/en-US/docs/Web/Reference/Events/load_(ProgressEvent)

// LoadEnd is a "loadend" Event
//   ProgressEvent - Progress has stopped (after "error", "abort", or "load" have
//   been dispatched)
//
// https://developer.mozilla.org/en-US/docs/Web/Events/loadend
func LoadEnd(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "loadend", Handler: handler}
}

// LoadStart is a "loadstart" Event
//   ProgressEvent - Progress has begun
//
// https://developer.mozilla.org/en-US/docs/Web/Events/loadstart
func LoadStart(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "loadstart", Handler: handler}
}

// Progress is a "progress" Event
//   ProgressEvent - In progress
//
// https://developer.mozilla.org/en-US/docs/Web/Events/progress
func Progress(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "progress", Handler: handler}
}

// "readystatechange" Event (duplicate name)
//   Event - The readyState attribute of a document has changed
//
// https://developer.mozilla.org/en-US/docs/Web/Events/readystatechange

// Timeout is a "timeout" Event
//   ProgressEvent - Progression is terminated due to preset time expiring.
//
// https://developer.mozilla.org/en-US/docs/Web/Events/timeout
func Timeout(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "timeout", Handler: handler}
}
