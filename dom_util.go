//go:build js && wasm

package mogowk

import (
	"syscall/js"
)

// SetTitle sets new title of the document
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/title
func SetTitle(title string) {
	document.Set("title", title)
}

// SetFavicon updates exists icon or sets new icon of the document
// imageType may be one of icon image type for example: image/x-icon, image/png, image/ico
//
// https://developer.mozilla.org/en-US/docs/Glossary/Favicon
func SetFavicon(imageType, url string) {
	link := document.Call("querySelector", "link[rel*='icon']")
	if link.IsNull() {
		link = document.Call("createElement", "link")
	}

	link.Set("type", imageType)
	link.Set("rel", "shortcut icon")
	link.Set("href", url)

	document.Get("head").Call("appendChild", link)
}

// AddStylesheet adds new stylesheet link to the document
//
// https://developer.mozilla.org/en-US/docs/Web/API/StyleSheet
func AddStylesheet(url string) {
	link := document.Call("createElement", "link")
	link.Set("rel", "stylesheet")
	link.Set("href", url)

	document.Get("head").Call("appendChild", link)
}

// SetEventsIntoWindow sets event listener to window object
//
// https://developer.mozilla.org/en-US/docs/Web/API/Window
func SetEventsIntoWindow(events ...*EventListener) {
	for _, event := range events {
		SetEventListenerInto(js.Global(), event)
	}
}

// SetEventsIntoDocument sets event listener to document object
//
// https://developer.mozilla.org/en-US/docs/Web/API/Document
func SetEventsIntoDocument(events ...*EventListener) {
	for _, event := range events {
		SetEventListenerInto(document, event)
	}
}
