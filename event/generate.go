//go:build ignore

package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"
	"text/template"
)

var (
	funcMap = template.FuncMap{
		"Wordwrap": wordwrap,
	}

	fileName = "event.gen.go"
)

// https://github.com/eidolon/wordwrap
func wordwrap(input string) string {
	var wrapped string
	// Split string into array of words
	words := strings.Fields(input)

	if len(words) == 0 {
		return wrapped
	}
	limit := 80
	remaining := limit

	for _, word := range words {
		if word == "__ln__" {
			wrapped += "\n//  "
			remaining = limit
			continue
		}

		if len(word)+1 > remaining {
			if len(wrapped) > 0 {
				wrapped += "\n//   "
			}
			wrapped += word
			remaining = limit - len(word)
		} else {
			if len(wrapped) > 0 {
				wrapped += " "
			}
			wrapped += word
			remaining = remaining - (len(word) + 1)
		}
	}
	return wrapped
}

type eventTmplData struct {
	EventName   string
	FuncName    string
	Description string
	Link        string
}

func main() {
	eventTmpl, err := template.New("event").Funcs(funcMap).Parse(`
// {{ .FuncName }} is a "{{ .EventName }}" Event
//   {{ .Description | Wordwrap }}
//
// {{ .Link }}
func {{ .FuncName }}(handler func(*mogowk.EventData)) *mogowk.EventListener {
	return &mogowk.EventListener{Name: "{{ .EventName }}", Handler: handler}
}
`)
	if err != nil {
		log.Fatalf("failed to Parse event template: %s", err)
	}

	duplicateNameEventTmpl, err := template.New("event").Funcs(funcMap).Parse(`
// "{{ .EventName }}" Event (duplicate name)
//   {{ .Description | Wordwrap }}
//
// {{ .Link }}
`)
	if err != nil {
		log.Fatalf("failed to Parse duplicate event template: %s", err)
	}

	sectionTmpl, err := template.New("section").Funcs(funcMap).Parse(`
//
// Specification: {{ .EventName }}
//   {{ .Description | Wordwrap }}
//
// {{ .Link }}
//
`)

	if err != nil {
		log.Fatalf("failed to Parse section template: %s", err)
	}

	file, err := os.Create(fileName)
	if err != nil {
		log.Fatalf("failed to create file: %s", err)
	}
	defer file.Close()

	_, err = fmt.Fprintln(file, `//go:build js && wasm
// Package event contains constructors of DOM Events.
// Generates with texts from https://developer.mozilla.org/en-US/docs/Web/Events page
package event

import (
	"gitlab.com/mogowk/mogowk"
)
`)

	if err != nil {
		log.Fatalf("failed to print file head: %s", err)
	}

	sameName := make(map[string]struct{}, len(events))

	for _, event := range events {
		_, ok := sameName[event.FuncName]

		switch {
		case ok:
			err = duplicateNameEventTmpl.Execute(file, event)

		case event.FuncName == "":
			err = sectionTmpl.Execute(file, event)

		case event.FuncName != "":
			err = eventTmpl.Execute(file, event)
			sameName[event.FuncName] = struct{}{}
		}

		if err != nil {
			log.Fatalf("failed to Execute template: %s", err)
		}
	}

	err = exec.Command("go", "fmt", fileName).Run()
	if err != nil {
		log.Fatalf("failed to fmt file: %s", err)
	}

	log.Printf("Success generated %s\n", fileName)
}

var events = []eventTmplData{
	// Clipboard
	{
		"Clipboard", "",
		"It provides operations for overriding the default clipboard actions (cut, copy and paste), and for directly accessing the clipboard contents",
		"https://www.w3.org/TR/clipboard-apis/#copy-event",
	},
	{
		"copy", "Copy",
		"ClipboardEvent - The text selection has been added to the clipboard",
		"https://developer.mozilla.org/en-US/docs/Web/Events/copy",
	},
	{
		"cut", "Cut",
		"ClipboardEvent - The text selection has been removed from the document and added to the clipboard",
		"https://developer.mozilla.org/en-US/docs/Web/Events/cut",
	},
	{
		"paste", "Paste",
		"ClipboardEvent - Data has been transferred from the system clipboard to the document",
		"https://developer.mozilla.org/en-US/docs/Web/Events/paste",
	},

	// CSS Animations
	{
		"CSS Animations", "",
		"The behavior of these keyframe animations can be controlled by specifying their duration, number of repeats, and repeating behavior",
		"https://www.w3.org/TR/css-animations-1/#animation-events",
	},
	{
		"animationcancel", "AnimationCancel",
		"AnimationEvent - A CSS animation has aborted",
		"https://developer.mozilla.org/en-US/docs/Web/Events/animationcancel",
	},
	{
		"animationend", "AnimationEnd",
		"AnimationEvent - A CSS animation has completed",
		"https://developer.mozilla.org/en-US/docs/Web/Events/animationend",
	},
	{
		"animationiteration", "AnimationIteration",
		"AnimationEvent - A CSS animation is repeated",
		"https://developer.mozilla.org/en-US/docs/Web/Events/animationiteration",
	},
	{
		"animationstart", "AnimationStart",
		"AnimationEvent - A CSS animation has started",
		"https://developer.mozilla.org/en-US/docs/Web/Events/animationstart",
	},

	{
		"transitionend", "TransitionEnd",
		"TransitionEvent - A CSS transition has completed.",
		"https://developer.mozilla.org/en-US/docs/Web/Events/transitionend",
	},

	// Device Orientation Events
	{
		"Device Orientation Events", "",
		"Defines several new DOM events that provide information about the physical orientation and motion of a hosting device",
		"https://w3c.github.io/deviceorientation/spec-source-orientation.html",
	},
	{
		"devicemotion", "DeviceMotion",
		"DeviceMotionEvent - Fresh data is available from a motion sensor",
		"https://developer.mozilla.org/en-US/docs/Web/Events/devicemotion",
	},
	{
		"deviceorientation", "DeviceOrientation",
		"DeviceOrientationEvent - Fresh data is available from an orientation sensor",
		"https://developer.mozilla.org/en-US/docs/Web/Events/deviceorientation",
	},

	// DOM
	{
		"DOM", "",
		`The definition of '"Mutation observers" and slotchange event' in that specification`,
		"https://dom.spec.whatwg.org/",
	},
	{
		"slotchange", "SlotChange",
		"The node contents of a HTMLSlotElement (<slot>) have changed.",
		"https://developer.mozilla.org/en-US/docs/Web/Events/slotchange",
	},

	// DOM L2
	{
		"DOM L2", "",
		"DOM Level 2 Event Model ... more in link",
		"https://www.w3.org/TR/DOM-Level-2-Events/events.html",
	},
	{
		"change", "Change",
		"Event - The change event is fired for <input>, <select>, and <textarea> elements when a change to the element's value is committed by the user",
		"https://developer.mozilla.org/en-US/docs/Web/Events/change",
	},
	{
		"reset", "Reset",
		"Event - A form is reset",
		"https://developer.mozilla.org/en-US/docs/Web/Events/reset",
	},
	{
		"submit", "Submit",
		"Event - A form is submitted",
		"https://developer.mozilla.org/en-US/docs/Web/Events/submit",
	},

	// DOM L3
	{
		"DOM L3", "",
		"Defines UI Events which extend the DOM Event objects defined in DOM. UI Events are those typically implemented by visual user agents for handling user interaction such as mouse and keyboard input.",
		"https://www.w3.org/TR/DOM-Level-3-Events/",
	},
	{
		"compositionend", "CompositionEnd",
		"CompositionEvent - The composition of a passage of text has been completed or canceled",
		"https://developer.mozilla.org/en-US/docs/Web/Events/compositionend",
	},
	{
		"compositionstart", "CompositionStart",
		"CompositionEvent - The composition of a passage of text is prepared (similar to keydown for a keyboard input, but works with other inputs such as speech recognition)",
		"https://developer.mozilla.org/en-US/docs/Web/Events/compositionstart",
	},
	{
		"compositionupdate", "CompositionUpdate",
		"CompositionEvent - A character is added to a passage of text being composed",
		"https://developer.mozilla.org/en-US/docs/Web/Events/compositionupdate",
	},
	{
		"blur", "Blur",
		"FocusEvent - An element has lost focus (does not bubble)",
		"https://developer.mozilla.org/en-US/docs/Web/Events/blur",
	},
	{
		"focus", "Focus",
		"FocusEvent - An element has received focus (does not bubble)",
		"https://developer.mozilla.org/en-US/docs/Web/Events/focus",
	},
	{
		"focusin", "FocusIn",
		"FocusEvent - An element is about to receive focus (bubbles)",
		"https://developer.mozilla.org/en-US/docs/Web/Events/focusin",
	},
	{
		"focusout", "FocusOut",
		"FocusEvent - An element is about to lose focus (bubbles)",
		"https://developer.mozilla.org/en-US/docs/Web/Events/focusout",
	},
	{
		"keydown", "KeyDown",
		"KeyboardEvent - A key is pressed down",
		"https://developer.mozilla.org/en-US/docs/Web/Events/keydown",
	},
	{
		"keyup", "KeyUp",
		"KeyboardEvent - A key is released",
		"https://developer.mozilla.org/en-US/docs/Web/Events/keyup",
	},
	{
		"click", "Click",
		"MouseEvent - A pointing device button has been pressed and released on an element",
		"https://developer.mozilla.org/en-US/docs/Web/Events/click",
	},
	{
		"dblclick", "DoubleClick",
		"MouseEvent - A pointing device button is clicked twice on an element",
		"https://developer.mozilla.org/en-US/docs/Web/Events/dblclick",
	},
	{
		"mousedown", "MouseDown",
		"MouseEvent - A pointing device button (usually a mouse) is pressed on an element",
		"https://developer.mozilla.org/en-US/docs/Web/Events/mousedown",
	},
	{
		"mouseenter", "MouseEnter",
		"MouseEvent - A pointing device is moved onto the element that has the listener attached",
		"https://developer.mozilla.org/en-US/docs/Web/Events/mouseenter",
	},
	{
		"mouseleave", "MouseLeave",
		"MouseEvent - A pointing device is moved off the element that has the listener attached",
		"https://developer.mozilla.org/en-US/docs/Web/Events/mouseleave",
	},
	{
		"mousemove", "MouseMove",
		"MouseEvent - A pointing device is moved over an element",
		"https://developer.mozilla.org/en-US/docs/Web/Events/mousemove",
	},
	{
		"mouseout", "MouseOut",
		"MouseEvent - A pointing device is moved off the element that has the listener attached or off one of its children",
		"https://developer.mozilla.org/en-US/docs/Web/Events/mouseout",
	},
	{
		"mouseover", "MouseOver",
		"MouseEvent - A pointing device is moved onto the element that has the listener attached or onto one of its children",
		"https://developer.mozilla.org/en-US/docs/Web/Events/mouseover",
	},
	{
		"mouseup", "MouseUp",
		"MouseEvent - A pointing device button is released over an element",
		"https://developer.mozilla.org/en-US/docs/Web/Events/mouseup",
	},
	{
		"abort", "Abort",
		`UIEvent - The loading of a resource has been aborted __ln__
Event (IndexedDB) - A transaction has been aborted __ln__
ProgressEvent (XMLHttpRequest) - Progression has been terminated (not due to an error)
`,

		`https://developer.mozilla.org/en-US/docs/Web/Events/abort
// https://developer.mozilla.org/en-US/docs/Web/Reference/Events/abort_indexedDB
// https://developer.mozilla.org/en-US/docs/Web/Reference/Events/abort_(ProgressEvent)`,
	},
	{
		"error", "Error",
		`UIEvent - A resource failed to load __ln__
Event (IndexedDB) - A request caused an error and failed __ln__
Event (Server Sent Events) - An event source connection has been failed __ln__
Event (Web Speech API) - A speech recognition error occurs __ln__
Event (SpeechSynthesisErrorEvent) - An error occurs that prevents the utterance from being successfully spoken __ln__
Event (WebSocket) - A WebSocket connection has been closed with prejudice __ln__
ProgressEvent (XMLHttpRequest) - Progression has failed
`,
		`https://developer.mozilla.org/en-US/docs/Web/Events/error
// https://developer.mozilla.org/en-US/docs/Web/Events/error_(SpeechRecognitionError)
// https://developer.mozilla.org/en-US/docs/Web/Events/error_(SpeechSynthesisError)`,
	},
	{
		"load", "Load",
		`UIEvent - A resource and its dependent resources have finished loading __ln__
ProgressEvent (XMLHttpRequest) - Progression has been successful
`,
		`https://developer.mozilla.org/en-US/docs/Web/Events/load
// https://developer.mozilla.org/en-US/docs/Web/Reference/Events/load_(ProgressEvent)`,
	},
	{
		"resize", "Resize",
		"UIEvent - The document view has been resized",
		"https://developer.mozilla.org/en-US/docs/Web/Events/resize",
	},
	{
		"scroll", "Scroll",
		"UIEvent - The document view or an element has been scrolled",
		"https://developer.mozilla.org/en-US/docs/Web/Events/scroll",
	},
	{
		"select", "Select",
		"UIEvent - Some text is being selected",
		"https://developer.mozilla.org/en-US/docs/Web/Events/select",
	},
	{
		"unload", "Unload",
		"UIEvent - The document or a dependent resource is being unloaded",
		"https://developer.mozilla.org/en-US/docs/Web/Events/unload",
	},
	{
		"wheel", "Wheel",
		"WheelEvent - A wheel button of a pointing device is rotated in any direction",
		"https://developer.mozilla.org/en-US/docs/Web/Events/wheel",
	},

	// Full Screen
	{
		"Full Screen", "",
		"Defines the fullscreen API for the web platform",
		"https://www.w3.org/TR/fullscreen/",
	},
	{
		"fullscreenchange", "FullScreenChange",
		"Event - An element was toggled to or from fullscreen mode",
		"https://developer.mozilla.org/en-US/docs/Web/Events/fullscreenchange",
	},
	{
		"fullscreenerror", "FullScreenError",
		"Event - It was impossible to switch to fullscreen mode for technical reasons or because the permission was denied",
		"https://developer.mozilla.org/en-US/docs/Web/Events/fullscreenerror",
	},

	// Gamepad
	{
		"Gamepad", "",
		"Defines a low-level interface that represents gamepad devices",
		"https://www.w3.org/TR/gamepad/",
	},
	{
		"gamepadconnected", "GamepadConnected",
		"GamepadEvent - A gamepad has been connected",
		"https://developer.mozilla.org/en-US/docs/Web/Events/gamepadconnected",
	},
	{
		"gamepaddisconnected", "GamepadDisconnected",
		"GamepadEvent - A gamepad has been disconnected",
		"https://developer.mozilla.org/en-US/docs/Web/Events/gamepaddisconnected",
	},
	// HTML 5.1
	{
		"HTML 5.1", "",
		"Defines the 2nd edition of the 5th major version, first minor revision of the core language (HTML)",
		"https://www.w3.org/TR/html51/#dom-navigator-languages",
	},
	{
		"languagechange", "LanguageChange",
		"The user's preferred languages have changed",
		"https://developer.mozilla.org/en-US/docs/Web/Events/languagechange",
	},

	// HTML5
	{
		"HTML5", "",
		"Defines the 5th major revision of the core language (HTML)",
		"https://www.w3.org/TR/html50/",
	},
	{
		"beforeunload", "BeforeUnload",
		"BeforeUnloadEvent - The window, the document and its resources are about to be unloaded",
		"https://developer.mozilla.org/en-US/docs/Web/Events/beforeunload",
	},
	{
		"drag", "Drag",
		"DragEvent - An element or text selection is being dragged (every 350ms)",
		"https://developer.mozilla.org/en-US/docs/Web/Events/drag",
	},
	{
		"dragend", "DragEnd",
		"DragEvent - A drag operation is being ended (by releasing a mouse button or hitting the escape key)",
		"https://developer.mozilla.org/en-US/docs/Web/Events/dragend",
	},
	{
		"dragenter", "DragEnter",
		"DragEvent - A dragged element or text selection enters a valid drop target",
		"https://developer.mozilla.org/en-US/docs/Web/Events/dragenter",
	},
	{
		"dragleave", "DragLeave",
		"DragEvent - A dragged element or text selection leaves a valid drop target",
		"https://developer.mozilla.org/en-US/docs/Web/Events/dragleave",
	},
	{
		"dragover", "DragOver",
		"DragEvent - An element or text selection is being dragged over a valid drop target __ln__ (fires every 350ms)",
		"https://developer.mozilla.org/en-US/docs/Web/Events/dragover",
	},
	{
		"dragstart", "DragStart",
		"DragEvent - The user starts dragging an element or text selection",
		"https://developer.mozilla.org/en-US/docs/Web/Events/dragstart",
	},
	{
		"drop", "Drop",
		"DragEvent - An element is dropped on a valid drop target",
		"https://developer.mozilla.org/en-US/docs/Web/Events/drop",
	},
	{
		"afterprint", "AfterPrint",
		"Event - The associated document has started printing or the print preview has been closed",
		"https://developer.mozilla.org/en-US/docs/Web/Events/afterprint",
	},
	{
		"beforeprint", "BeforePrint",
		"Event - The associated document is about to be printed or previewed for printing",
		"https://developer.mozilla.org/en-US/docs/Web/Events/beforeprint",
	},
	{
		"DOMContentLoaded", "DOMContentLoaded",
		"Event - The document has finished loading (but not its dependent resources)",
		"https://developer.mozilla.org/en-US/docs/Web/Events/DOMContentLoaded",
	},
	{
		"input", "Input",
		"Event - The value of an element changes or the content of an element with the attribute contenteditable is modified",
		"https://developer.mozilla.org/en-US/docs/Web/Events/input",
	},
	{
		"invalid", "Invalid",
		"Event - A submittable element has been checked and doesn't satisfy its constraints",
		"https://developer.mozilla.org/en-US/docs/Web/Events/invalid",
	},
	{
		"hashchange", "HashChange",
		"HashChangeEvent - The fragment identifier of the URL has changed (the part of the URL after the #)",
		"https://developer.mozilla.org/en-US/docs/Web/Events/hashchange",
	},
	{
		"contextmenu", "ContextMenu",
		"MouseEvent - The right button of the mouse is clicked (before the context menu is displayed)",
		"https://developer.mozilla.org/en-US/docs/Web/Events/contextmenu",
	},
	{
		"show", "Show",
		"MouseEvent - A contextmenu event was fired on (or bubbled to) an element that has a contextmenu attribute",
		"https://developer.mozilla.org/en-US/docs/Web/Events/show",
	},
	{
		"pagehide", "PageHide",
		"PageTransitionEvent - A session history entry is being traversed from",
		"https://developer.mozilla.org/en-US/docs/Web/Events/pagehide",
	},
	{
		"pageshow", "PageShow",
		"PageTransitionEvent - A session history entry is being traversed to",
		"https://developer.mozilla.org/en-US/docs/Web/Events/pageshow",
	},
	{
		"popstate", "PopState",
		"PopStateEvent - Fired when the active history entry changes while the user navigates the session history",
		"https://developer.mozilla.org/en-US/docs/Web/Events/popstate",
	},
	{
		"readystatechange", "ReadyStateChange",
		`Event - The readyState attribute of a document has changed __ln__
Event (XMLHttpRequest) - The readyState attribute of a document has changed`,
		`https://developer.mozilla.org/en-US/docs/Web/Events/readystatechange
// https://developer.mozilla.org/en-US/docs/Web/Events/readystatechange`,
	},

	// HTML5 media
	{
		"HTML5 media", "",
		"Used to present audio data, or video and audio data, to the user. This is referred to as media data in this section, since this section applies equally to media elements for audio or for video",
		"https://www.w3.org/TR/html50/embedded-content-0.html#media-element",
	},
	{
		"canplay", "CanPlay",
		"Event - The user agent can play the media, but estimates that not enough data has been loaded to play the media up to its end without having to stop for further buffering of content",
		"https://developer.mozilla.org/en-US/docs/Web/Events/canplay",
	},
	{
		"canplaythrough", "CanPlayThrough",
		"Event - The user agent can play the media up to its end without having to stop for further buffering of content",
		"https://developer.mozilla.org/en-US/docs/Web/Events/canplaythrough",
	},
	{
		"durationchange", "DurationChange",
		"Event - The duration attribute has been updated",
		"https://developer.mozilla.org/en-US/docs/Web/Events/durationchange",
	},
	{
		"emptied", "Emptied",
		"Event - The media has become empty For example, this event is triggered if the media has already been loaded (or partially loaded), and the load() method is called to reload it",
		"https://developer.mozilla.org/en-US/docs/Web/Events/emptied",
	},
	{
		"ended", "Ended",
		`Event - Playback has stopped because the end of the media was reached __ln__
Event - (Web Audio API) Playback has stopped because the end of the media was reached
`,
		`https://developer.mozilla.org/en-US/docs/Web/Events/ended
// https://developer.mozilla.org/en-US/docs/Web/Events/ended_(Web_Audio)`,
	},
	{
		"loadeddata", "LoadedData",
		"Event - The first frame of the media has finished loading",
		"https://developer.mozilla.org/en-US/docs/Web/Events/loadeddata",
	},
	{
		"loadedmetadata", "LoadedMetaData",
		"Event - The metadata has been loaded",
		"https://developer.mozilla.org/en-US/docs/Web/Events/loadedmetadata",
	},
	{
		"pause", "Pause",
		`Event - Playback has been paused __ln__
SpeechSynthesisEvent (Web Speech API) - The utterance is paused part way through
`,
		`https://developer.mozilla.org/en-US/docs/Web/Events/pause
// https://developer.mozilla.org/en-US/docs/Web/Events/pause_(SpeechSynthesis)`,
	},
	{
		"play", "Play",
		"Event - Playback has begun",
		"https://developer.mozilla.org/en-US/docs/Web/Events/play",
	},
	{
		"playing", "Playing",
		"Event - Playback is ready to start after having been paused or delayed due to lack of data",
		"https://developer.mozilla.org/en-US/docs/Web/Events/playing",
	},
	{
		"ratechange", "RateChange",
		"Event - The playback rate has changed",
		"https://developer.mozilla.org/en-US/docs/Web/Events/ratechange",
	},
	{
		"seeked", "Seeked",
		"Event - A seek operation completed",
		"https://developer.mozilla.org/en-US/docs/Web/Events/seeked",
	},
	{
		"seeking", "Seeking",
		"Event - A seek operation began",
		"https://developer.mozilla.org/en-US/docs/Web/Events/seeking",
	},
	{
		"stalled", "Stalled",
		"Event - The user agent is trying to fetch media data, but data is unexpectedly not forthcoming",
		"https://developer.mozilla.org/en-US/docs/Web/Events/stalled",
	},
	{
		"suspend", "Suspend",
		"Event - Media data loading has been suspended",
		"https://developer.mozilla.org/en-US/docs/Web/Events/suspend",
	},
	{
		"timeupdate", "TimeUpdate",
		"Event - The time indicated by the currentTime attribute has been updated",
		"https://developer.mozilla.org/en-US/docs/Web/Events/timeupdate",
	},
	{
		"volumechange", "VolumeChange",
		"Event - The volume has changed",
		"https://developer.mozilla.org/en-US/docs/Web/Events/volumechange",
	},
	{
		"waiting", "Waiting",
		"Event - Playback has stopped because of a temporary lack of data",
		"https://developer.mozilla.org/en-US/docs/Web/Events/waiting",
	},
	{
		"offline", "Offline",
		"Event - The browser has lost access to the network",
		"https://developer.mozilla.org/en-US/docs/Web/Events/offline",
	},
	{
		"online", "Online",
		"Event - The browser has gained access to the network (but particular websites might be unreachable)",
		"https://developer.mozilla.org/en-US/docs/Web/Events/online",
	},

	// IndexedDB
	{
		"IndexedDB", "",
		"Defines APIs for a database of records holding simple values and hierarchical objects. Each record consists of a key and some value",
		"https://www.w3.org/TR/IndexedDB/",
	},
	{
		"abort", "Abort",
		"Event - A transaction has been aborted",
		"https://developer.mozilla.org/en-US/docs/Web/Reference/Events/abort_indexedDB",
	},
	{
		"error", "Error",
		"Event - A request caused an error and failed",
		"https://developer.mozilla.org/en-US/docs/Web/Events/error",
	},
	{
		"success", "Success",
		"Event - A request successfully completed",
		"https://developer.mozilla.org/en-US/docs/Web/Reference/Events/success_indexedDB",
	},
	{
		"blocked", "Blocked",
		"Event - An open connection to a database is blocking a versionchange transaction on the same database",
		"https://developer.mozilla.org/en-US/docs/Web/Reference/Events/blocked_indexedDB",
	},
	{
		"complete", "Complete",
		`Event - A transaction successfully completed __ln__
OfflineAudioCompletionEvent (Web Audio API) - The rendering of an OfflineAudioContext is terminated",
`,
		`https://developer.mozilla.org/en-US/docs/Web/Reference/Events/complete_indexedDB
// https://developer.mozilla.org/en-US/docs/Web/Events/complete`,
	},
	{
		"upgradeneeded", "UpgradeNeeded",
		"Event - An attempt was made to open a database with a version number higher than its current version A versionchange transaction has been created",
		"https://developer.mozilla.org/en-US/docs/Web/Reference/Events/upgradeneeded_indexedDB",
	},
	{
		"versionchange", "VersionChange",
		"Event - A versionchange transaction completed",
		"https://developer.mozilla.org/en-US/docs/Web/Reference/Events/versionchange_indexedDB",
	},

	// Media Capture and Streams
	{
		"Media Capture and Streams", "",
		"Defines a set of JavaScript APIs that allow local media, including audio and video, to be requested from a platform",
		"https://w3c.github.io/mediacapture-main/",
	},
	{
		"devicechange", "DeviceChange",
		"Event - A media device such as a camera, microphone, or speaker is connected or removed from the system",
		"https://developer.mozilla.org/en-US/docs/Web/Events/devicechange",
	},

	// Notifications API
	{
		"Notifications API", "",
		"Defines an API for end-user notifications. A notification allows alerting the user outside the context of a web page of an occurrence, such as the delivery of email",
		"https://www.w3.org/TR/notifications/",
	},
	{
		"notificationclick", "NotificationClick",
		"NotificationEvent - A system notification spawned by ServiceWorkerRegistrationshowNotification() has been clicked",
		"https://developer.mozilla.org/en-US/docs/Web/Events/notificationclick",
	},

	// Page visibility
	{
		"Page visibility", "",
		"Defines a means to programmatically determine the visibility state of a document. This can aid in the development of resource efficient web applications",
		"http://www.w3.org/TR/page-visibility",
	},
	{
		"visibilitychange", "VisibilityChange",
		"Event - The content of a tab has become visible or has been hidden",
		"https://developer.mozilla.org/en-US/docs/Web/Events/visibilitychange",
	},

	// Pointer Events
	{
		"Pointer Events", "",
		"Describes events and related interfaces for handling hardware agnostic pointer input from devices including a mouse, pen, touchscreen, etc",
		"https://www.w3.org/TR/pointerevents/#the-gotpointercapture-event",
	},
	{
		"gotpointercapture", "GotPointerCapture",
		"PointerEvent - Element receives pointer capture",
		"https://developer.mozilla.org/en-US/docs/Web/Events/gotpointercapture",
	},
	{
		"lostpointercapture", "LostPointerCapture",
		"PointerEvent - Element lost pointer capture",
		"https://developer.mozilla.org/en-US/docs/Web/Events/lostpointercapture",
	},
	{
		"pointercancel", "PointerCancel",
		"PointerEvent - The pointer is unlikely to produce any more events",
		"https://developer.mozilla.org/en-US/docs/Web/Events/pointercancel",
	},
	{
		"pointerdown", "PointerDown",
		"PointerEvent - The pointer enters the active buttons state",
		"https://developer.mozilla.org/en-US/docs/Web/Events/pointerdown",
	},
	{
		"pointerenter", "PointerEnter",
		"PointerEvent - Pointing device is moved inside the hit-testing boundary",
		"https://developer.mozilla.org/en-US/docs/Web/Events/pointerenter",
	},
	{
		"pointerleave", "PointerLeave",
		"PointerEvent - Pointing device is moved out of the hit-testing boundary",
		"https://developer.mozilla.org/en-US/docs/Web/Events/pointerleave",
	},
	{
		"pointermove", "PointerMove",
		"PointerEvent - The pointer changed coordinates",
		"https://developer.mozilla.org/en-US/docs/Web/Events/pointermove",
	},
	{
		"pointerout", "PointerOut",
		"PointerEvent - The pointing device moved out of hit-testing boundary or leaves detectable hover range",
		"https://developer.mozilla.org/en-US/docs/Web/Events/pointerout",
	},
	{
		"pointerover", "PointerOver",
		"PointerEvent - The pointing device is moved into the hit-testing boundary",
		"https://developer.mozilla.org/en-US/docs/Web/Events/pointerover",
	},
	{
		"pointerup", "PointerUp",
		"PointerEvent - The pointer leaves the active buttons state",
		"https://developer.mozilla.org/en-US/docs/Web/Events/pointerup",
	},

	// Pointer Lock
	{
		"Pointer Lock", "",
		"Defines an API that provides scripted access to raw mouse movement data while locking the target of mouse events to a single element and removing the cursor from view",
		"https://www.w3.org/TR/pointerlock/",
	},
	{
		"pointerlockchange", "PointerLockChange",
		"Event - The pointer was locked or released",
		"https://developer.mozilla.org/en-US/docs/Web/Events/pointerlockchange",
	},
	{
		"pointerlockerror", "PointerLockError",
		"Event - It was impossible to lock the pointer for technical reasons or because the permission was denied",
		"https://developer.mozilla.org/en-US/docs/Web/Events/pointerlockerror",
	},

	// Push API
	{
		"Push API", "",
		"Enables sending of a push message to a web application via a push service",
		"https://w3c.github.io/push-api/",
	},
	{
		"push", "Push",
		"PushEvent - A Service Worker has received a push message",
		"https://developer.mozilla.org/en-US/docs/Web/Events/push",
	},
	{
		"pushsubscriptionchange", "PushSubscriptionChange",
		"PushEvent - A PushSubscription has expired",
		"https://developer.mozilla.org/en-US/docs/Web/Events/pushsubscriptionchange",
	},

	// Resource Timing
	{
		"Resource Timing", "",
		"Defines an interface for web applications to access the complete timing information for resources in a document",
		"https://w3c.github.io/resource-timing/",
	},
	{
		"resourcetimingbufferfull", "ResourceTimingBufferFull",
		"Performance - The browser's resource timing buffer is full",
		"https://developer.mozilla.org/en-US/docs/Web/Events/resourcetimingbufferfull",
	},

	// Screen Orientation
	{
		"Screen Orientation", "",
		"Provides the ability to read the screen orientation type and angle, to be informed when the screen orientation changes, and to lock the screen to a specific orientation",
		"https://www.w3.org/TR/screen-orientation/",
	},
	{
		"orientationchange", "OrientationChange",
		"Event - The orientation of the device (portrait/landscape) has changed",
		"https://developer.mozilla.org/en-US/docs/Web/Events/orientationchange",
	},

	// Selection API
	{
		"Selection API", "",
		"Defines APIs for selection, which allows users and authors to select a portion of a document or specify a point of interest for copy, paste, and other editing operations",
		"https://w3c.github.io/selection-api/",
	},
	{
		"selectionchange", "SelectionChange",
		"Event - The selection in the document has been changed",
		"https://developer.mozilla.org/en-US/docs/Web/API/Document/selectionchange_event",
	},
	{
		"selectstart", "SelectStart",
		"Event - A selection just started",
		"https://developer.mozilla.org/en-US/docs/Web/API/Document/selectstart_event",
	},

	// Server Sent Events
	{
		"Server Sent Events", "",
		"Defines an API for opening an HTTP connection for receiving push notifications from a server in the form of DOM events",
		"https://www.w3.org/TR/eventsource/",
	},
	{
		"error", "Error",
		"Event - An event source connection has been failed",
		"https://developer.mozilla.org/en-US/docs/Web/Events/error",
	},
	{
		"open", "Open",
		`Event - An event source connection has been established __ln__
Event (WebSocket) - A WebSocket connection has been established
`,
		`https://developer.mozilla.org/en-US/docs/Web/Reference/Events/open_serversentevents
// https://developer.mozilla.org/en-US/docs/Web/Reference/Events/open_websocket`,
	},
	{
		"message", "Message",
		`MessageEvent - A message is received through an event source __ln__
ServiceWorkerMessageEvent (Service Workers) - A message is received from a service worker, or a message is received in a service worker from another context __ln__
MessageEvent (Web Workers) - A message is received from a Web Worker __ln__
MessageEvent (WebSocket) - A message is received through a WebSocket
`,
		`https://developer.mozilla.org/en-US/docs/Web/Reference/Events/message_serversentevents
// https://developer.mozilla.org/en-US/docs/Web/Events/message_(ServiceWorker)
// https://developer.mozilla.org/en-US/docs/Web/Reference/Events/message_webworker
// https://developer.mozilla.org/en-US/docs/Web/Reference/Events/message_websocket`,
	},

	// Service Workers
	{
		"Service Workers", "",
		"Service workers essentially act as proxy servers that sit between web applications, the browser, and the network (when available)",
		"https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API",
	},
	{
		"message", "Message",
		"ServiceWorkerMessageEvent - A message is received from a service worker, or a message is received in a service worker from another context",
		"https://developer.mozilla.org/en-US/docs/Web/Events/message_(ServiceWorker)",
	},

	// SVG
	{
		"SVG", "",
		"Describes information about events, including under which circumstances events are triggered and how to indicate whether a given document can be zoomed and panned",
		"https://www.w3.org/TR/SVG/interact.html",
	},
	{
		"SVGAbort", "SVGAbort",
		"SVGEvent - Page loading has been stopped before the SVG was loaded",
		"https://developer.mozilla.org/en-US/docs/Web/Events/SVGAbort",
	},
	{
		"SVGError", "SVGError",
		"SVGEvent - An error has occurred before the SVG was loaded",
		"https://developer.mozilla.org/en-US/docs/Web/Events/SVGError",
	},
	{
		"SVGLoad", "SVGLoad",
		"SVGEvent - An SVG document has been loaded and parsed",
		"https://developer.mozilla.org/en-US/docs/Web/Events/SVGLoad",
	},
	{
		"SVGResize", "SVGResize",
		"SVGEvent - An SVG document is being resized",
		"https://developer.mozilla.org/en-US/docs/Web/Events/SVGResize",
	},
	{
		"SVGScroll", "SVGScroll",
		"SVGEvent - An SVG document is being scrolled",
		"https://developer.mozilla.org/en-US/docs/Web/Events/SVGScroll",
	},
	{
		"SVGUnload", "SVGUnload",
		"SVGEvent - An SVG document has been removed from a window or frame",
		"https://developer.mozilla.org/en-US/docs/Web/Events/SVGUnload",
	},
	{
		"SVGZoom", "SVGZoom",
		"SVGZoomEvent - An SVG document is being zoomed",
		"https://developer.mozilla.org/en-US/docs/Web/Events/SVGZoom",
	},
	{
		"beginEvent", "BeginEvent",
		"TimeEvent - A SMIL animation element begins",
		"https://developer.mozilla.org/en-US/docs/Web/Events/beginEvent",
	},
	{
		"endEvent", "EndEvent",
		"TimeEvent - A SMIL animation element ends",
		"https://developer.mozilla.org/en-US/docs/Web/Events/endEvent",
	},
	{
		"repeatEvent", "RepeatEvent",
		"TimeEvent - A SMIL animation element is repeated",
		"https://developer.mozilla.org/en-US/docs/Web/Events/repeatEvent",
	},

	// Touch Events
	{
		"Touch Events", "",
		"Defines a set of low-level events that represent one or more points of contact with a touch-sensitive surface",
		"https://www.w3.org/TR/touch-events/",
	},
	{
		"touchcancel", "TouchCancel",
		"TouchEvent - A touch point has been disrupted in an implementation-specific manner (too many touch points, for example)",
		"https://developer.mozilla.org/en-US/docs/Web/Events/touchcancel",
	},
	{
		"touchend", "TouchEnd",
		"TouchEvent - A touch point is removed from the touch surface",
		"https://developer.mozilla.org/en-US/docs/Web/Events/touchend",
	},
	{
		"touchmove", "TouchMove",
		"TouchEvent - A touch point is moved along the touch surface",
		"https://developer.mozilla.org/en-US/docs/Web/Events/touchmove",
	},
	{
		"touchstart", "TouchStart",
		"TouchEvent - A touch point is placed on the touch surface",
		"https://developer.mozilla.org/en-US/docs/Web/Events/touchstart",
	},

	// Web Audio API
	{
		"Web Audio API", "",
		"Describes a high-level Web API for processing and synthesizing audio in web applications",
		"https://webaudio.github.io/web-audio-api/",
	},
	{
		"ended", "Ended",
		"Event - Playback has stopped because the end of the media was reached",
		"https://developer.mozilla.org/en-US/docs/Web/Events/ended_(Web_Audio)",
	},
	{
		"complete", "Complete",
		"OfflineAudioCompletionEvent - The rendering of an OfflineAudioContext is terminated",
		"https://developer.mozilla.org/en-US/docs/Web/Events/complete",
	},

	// Web Speech API
	{
		"Web Speech API", "",
		"Defines a JavaScript API to enable web developers to incorporate speech recognition and synthesis into their web pages",
		"https://wicg.github.io/speech-api/",
	},
	{
		"audioend", "AudioEnd",
		"Event - The user agent has finished capturing audio for speech recognition",
		"https://developer.mozilla.org/en-US/docs/Web/Events/audioend",
	},
	{
		"audiostart", "AudioStart",
		"Event - The user agent has started to capture audio for speech recognition",
		"https://developer.mozilla.org/en-US/docs/Web/Events/audiostart",
	},
	{
		"end", "End",
		`Event (Web Speech API) - The speech recognition service has disconnected __ln__
SpeechSynthesisEvent (Web Speech API) - The utterance has finished being spoken`,
		`https://developer.mozilla.org/en-US/docs/Web/Events/end_(SpeechRecognition)
// https://developer.mozilla.org/en-US/docs/Web/Events/end_(SpeechSynthesis)`,
	},
	{
		"error", "Error",
		"Event - A speech recognition error occurs",
		"https://developer.mozilla.org/en-US/docs/Web/Events/error_(SpeechRecognitionError)",
	},
	{
		"soundend", "SoundEnd",
		"Event - Any sound — recognizable speech or not — has stopped being detected",
		"https://developer.mozilla.org/en-US/docs/Web/Events/soundend",
	},
	{
		"soundstart", "SoundStart",
		"Event - Any sound — recognizable speech or not — has been detected",
		"https://developer.mozilla.org/en-US/docs/Web/Events/soundstart",
	},
	{
		"speechend", "SpeechEnd",
		"Event - Speech recognized by the speech recognition service has stopped being detected",
		"https://developer.mozilla.org/en-US/docs/Web/Events/speechend",
	},
	{
		"speechstart", "SpeechStart",
		"Event - Sound that is recognized by the speech recognition service as speech has been detected",
		"https://developer.mozilla.org/en-US/docs/Web/Events/speechstart",
	},
	{
		"start", "Start",
		`Event - The speech recognition service has begun listening to incoming audio with intent to recognize grammars associated with the current SpeechRecognition __ln__
SpeechSynthesisEvent (Web Speech API) - The utterance has begun to be spoken
`,
		`https://developer.mozilla.org/en-US/docs/Web/Events/start_(SpeechRecognition)
// https://developer.mozilla.org/en-US/docs/Web/Events/start_(SpeechSynthesis)`,
	},
	{
		"voiceschanged", "VoicesChanged",
		"Event - The list of SpeechSynthesisVoice objects that would be returned by the SpeechSynthesisgetVoices() method has changed (when the voiceschanged event fires)",
		"https://developer.mozilla.org/en-US/docs/Web/Events/voiceschanged",
	},
	{
		"nomatch", "NoMatch",
		"SpeechRecognitionEvent - The speech recognition service returns a final result with no significant recognition",
		"https://developer.mozilla.org/en-US/docs/Web/Events/nomatch",
	},
	{
		"result", "Result",
		"SpeechRecognitionEvent - The speech recognition service returns a result — a word or phrase has been positively recognized and this has been communicated back to the app",
		"https://developer.mozilla.org/en-US/docs/Web/Events/result",
	},
	{
		"error", "Error",
		"SpeechSynthesisErrorEvent - An error occurs that prevents the utterance from being successfully spoken",
		"https://developer.mozilla.org/en-US/docs/Web/Events/error_(SpeechSynthesisError)",
	},
	{
		"boundary", "Boundary",
		"SpeechSynthesisEvent - The spoken utterance reaches a word or sentence boundary",
		"https://developer.mozilla.org/en-US/docs/Web/Events/boundary",
	},
	{
		"end", "End",
		"SpeechSynthesisEvent - The utterance has finished being spoken",
		"https://developer.mozilla.org/en-US/docs/Web/Events/end_(SpeechSynthesis)",
	},
	{
		"mark", "Mark",
		`SpeechSynthesisEvent - The spoken utterance reaches a named SSML "mark" tag`,
		"https://developer.mozilla.org/en-US/docs/Web/Events/mark",
	},
	{
		"pause", "Pause",
		"SpeechSynthesisEvent - The utterance is paused part way through",
		"https://developer.mozilla.org/en-US/docs/Web/Events/pause_(SpeechSynthesis)",
	},
	{
		"resume", "Resume",
		"SpeechSynthesisEvent - A paused utterance is resumed",
		"https://developer.mozilla.org/en-US/docs/Web/Events/resume",
	},
	{
		"start", "Start",
		"SpeechSynthesisEvent - The utterance has begun to be spoken",
		"https://developer.mozilla.org/en-US/docs/Web/Events/start_(SpeechSynthesis)",
	},

	// Web Storage
	{
		"Web Storage", "",
		"Defines an API for persistent data storage of key-value pair data in Web clients",
		"https://www.w3.org/TR/webstorage",
	},
	{
		"storage", "Storage",
		"StorageEvent - A storage area (localStorage or sessionStorage) has changed",
		"https://developer.mozilla.org/en-US/docs/Web/Events/storage",
	},

	// Web Workers
	{
		"Web Workers", "",
		"Defines an API that allows Web application authors to spawn background workers running scripts in parallel to their main page",
		"https://www.w3.org/TR/workers/",
	},
	{
		"message", "Message",
		"MessageEvent - A message is received from a Web Worker",
		"https://developer.mozilla.org/en-US/docs/Web/Reference/Events/message_webworker",
	},

	// WebSocket
	{
		"WebSocket", "",
		"Defines an API that enables Web pages to use the WebSocket protocol (defined by the IETF) for two-way communication with a remote host",
		"https://www.w3.org/TR/websockets/",
	},
	{
		"close", "Close",
		"Event - A WebSocket connection has been closed",
		"https://developer.mozilla.org/en-US/docs/Web/Reference/Events/close_websocket",
	},
	{
		"error", "Error",
		"Event - A WebSocket connection has been closed with prejudice (some data couldn't be sent for example)",
		"https://developer.mozilla.org/en-US/docs/Web/Events/error",
	},
	{
		"open", "Open",
		"Event - A WebSocket connection has been established",
		"https://developer.mozilla.org/en-US/docs/Web/Reference/Events/open_websocket",
	},
	{
		"message", "Message",
		"MessageEvent - A message is received through a WebSocket",
		"https://developer.mozilla.org/en-US/docs/Web/Reference/Events/message_websocket",
	},
	{
		"messageerror", "MessageError",
		"MessageEvent - A message error is raised when a message is received by an object",
		"https://developer.mozilla.org/en-US/docs/Web/Events/messageerror",
	},

	// XMLHttpRequest
	{
		"XMLHttpRequest", "",
		"Defines an API that provides scripted client functionality for transferring data between a client and a server",
		"https://www.w3.org/TR/XMLHttpRequest/",
	},
	{
		"abort", "Abort",
		"ProgressEvent - Progression has been terminated (not due to an error)",
		"https://developer.mozilla.org/en-US/docs/Web/Reference/Events/abort_(ProgressEvent)",
	},
	{
		"error", "Error",
		"ProgressEvent - Progression has failed",
		"https://developer.mozilla.org/en-US/docs/Web/Events/error",
	},
	{
		"load", "Load",
		"ProgressEvent - Progression has been successful",
		"https://developer.mozilla.org/en-US/docs/Web/Reference/Events/load_(ProgressEvent)",
	},
	{
		"loadend", "LoadEnd",
		`ProgressEvent - Progress has stopped (after "error", "abort", or "load" have been dispatched)`,
		"https://developer.mozilla.org/en-US/docs/Web/Events/loadend",
	},
	{
		"loadstart", "LoadStart",
		"ProgressEvent - Progress has begun",
		"https://developer.mozilla.org/en-US/docs/Web/Events/loadstart",
	},
	{
		"progress", "Progress",
		"ProgressEvent - In progress",
		"https://developer.mozilla.org/en-US/docs/Web/Events/progress",
	},
	{
		"readystatechange", "ReadyStateChange",
		"Event - The readyState attribute of a document has changed",
		"https://developer.mozilla.org/en-US/docs/Web/Events/readystatechange",
	},
	{
		"timeout", "Timeout",
		"ProgressEvent - Progression is terminated due to preset time expiring.",
		"https://developer.mozilla.org/en-US/docs/Web/Events/timeout",
	},
}
