//go:build js && wasm

package mogowk

import (
	"syscall/js"
)

// EventData contains target and other arguments of an event.
type EventData struct {
	Target js.Value
	Args   []js.Value
}

// EventListener represents js event listener.
type EventListener struct {
	Name              string
	Handler           func(*EventData)
	isPreventDefault  bool
	isStopPropagation bool
}

// PreventDefault tells the user agent that if the event does not get explicitly handled,
// its default action should not be taken as it normally would be.
//
// https://developer.mozilla.org/en-US/docs/Web/API/Event/preventDefault
func (e *EventListener) PreventDefault() *EventListener {
	e.isPreventDefault = true
	return e
}

// StopPropagation prevents further propagation of the current event
// in the capturing and bubbling phases
//
// https://developer.mozilla.org/en-US/docs/Web/API/Event/stopPropagation
func (e *EventListener) StopPropagation() *EventListener {
	e.isStopPropagation = true
	return e
}

// SetEventListenerInto adds event listener to js element.
func SetEventListenerInto(element js.Value, event *EventListener) {
	element.Call(
		"addEventListener",
		event.Name,
		js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
			jsEvent := args[0]
			if event.isPreventDefault {
				jsEvent.Call("preventDefault")
			}
			if event.isStopPropagation {
				jsEvent.Call("stopPropagation")
			}

			event.Handler(&EventData{
				Target: jsEvent.Get("target"),
				Args:   args,
			})

			return js.Undefined()
		}),
	)
}
