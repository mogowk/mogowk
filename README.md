# mogowk

Go WASM Frontend render

## About

* Golang Fronted HTML render (GoFrenDer)
* Build your frontend app with your custom components
* Without magic. It works as you expect
* Sets of the HTML tags and event listeners
* Experimental lib. API may be changed

## Install

Use go modules

`go get gitlab.com/mogowk/mogowk`

## Hello world example

```go
package main

import (
	"gitlab.com/mogowk/mogowk"
	"gitlab.com/mogowk/mogowk/tag"
)

// Page is your custom component
type Page struct {
	mogowk.Component // must to be embedded

	text string
}

// Render implements of the mogowk.Renderer interface
// This is rander of your component
func (p *Page) Render() *mogowk.Tag {
	return tag.Body().WithChilds(
		tag.H1().WithChilds(tag.Text(p.text)),
	)
}

func main() {
	mogowk.SetTitile("Mogowk")
	p := &Page{text: "Hello mogowk"}
	mogowk.RenderBody(p)
}
```

### Run it

**Install wasmserve**

`go get -u github.com/hajimehoshi/wasmserve`

**Run wasm local server**
`wasmserve ./main.go`

**Open** [http://localhost:8080/](http://localhost:8080/)

See more examples in [mogowk_examples](https://gitlab.com/mogowk/mogowk_examples)
