//go:build js && wasm

// Package mogowk contains base types and methods of the project.
package mogowk

import (
	"syscall/js"
)

var document = js.Global().Get("document")

// Component is a built-in type for user's custom components
//
// For Example:
//   type MyComponent struct {
//       mogowk.Component // <- embedded
//       Field1 string
//   }
// It's required.
type Component struct {
	node js.Value
}

// getComponent implements Renderer interface as a Component.
// nolint:unused // it is interface implementation method.
func (c *Component) getComponent() *Component {
	return c
}

// Renderer returns Tag for compiling into js element
// and may be a Component.
type Renderer interface {
	Render() *Tag

	getComponent() *Component
}

// render compiles Tags or Components with its children
// and returns result also sets a js node of a component.
func render(r Renderer) js.Value {
	tag := r.Render()
	node := tag.compileElement()

	if c := r.getComponent(); c != nil {
		c.node = node
	}

	for _, child := range tag.Children {
		nodeChild := render(child)
		node.Call("appendChild", nodeChild)
	}

	return node
}

// RenderIntoDomElem renders Component into your special DOM element.
func RenderIntoDomElem(oldNode js.Value, r Renderer) {
	newNode := render(r)
	oldNode.Get("parentNode").Call("replaceChild", newNode, oldNode)
}

// RenderIntoTag renders Component into first HTML tag.
func RenderIntoTag(tagName string, r Renderer) {
	tag := document.Call("querySelector", tagName)
	RenderIntoDomElem(tag, r)
}

// RenderBody renders root Component that returns a body html tag.
func RenderBody(r Renderer) {
	RenderIntoTag("body", r)

	select {} // Go WASM have to work forever.
}

// Rerender rerenders rendered component only.
func Rerender(r Renderer) {
	c := r.getComponent()
	if c == nil {
		panic("mogowk.Rerender: only components can be rerendered")
	}

	if c.node.IsNull() {
		panic("mogowk.Rerender: component never has been rendered")
	}

	oldNode := c.node
	newNode := render(r)

	oldNode.Get("parentNode").Call("replaceChild", newNode, oldNode)
}
