//go:build js && wasm

// Package tag contains constructors of HTML Tags.
// Generates with texts from https://developer.mozilla.org/en-US/docs/Web/HTML/Element page
package tag

import (
	"gitlab.com/mogowk/mogowk"
)

// Body is a <body> HTML tag
//   Defines the document's body
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/body
func Body() *mogowk.Tag {
	return &mogowk.Tag{Name: "BODY"}
}

// Link is a <link> HTML tag
//   Specifies relationships between the current document and an external resource
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/link
func Link() *mogowk.Tag {
	return &mogowk.Tag{Name: "LINK"}
}

// Style is a <style> HTML tag
//   Contains style information for a document, or part of a document
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/style
func Style() *mogowk.Tag {
	return &mogowk.Tag{Name: "STYLE"}
}

// Address is a <address> HTML tag
//   Indicates that the enclosed HTML provides contact information
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/address
func Address() *mogowk.Tag {
	return &mogowk.Tag{Name: "ADDRESS"}
}

// Article is a <article> HTML tag
//   Represents a self-contained composition in a document, page, etc...
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/article
func Article() *mogowk.Tag {
	return &mogowk.Tag{Name: "ARTICLE"}
}

// Aside is a <aside> HTML tag
//   Represents a portion of a document whose content is only indirectly related to
//   the document's main content
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/aside
func Aside() *mogowk.Tag {
	return &mogowk.Tag{Name: "ASIDE"}
}

// Header is a <header> HTML tag
//   Represents introductory content, typically a group of introductory or
//   navigational aids
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/header
func Header() *mogowk.Tag {
	return &mogowk.Tag{Name: "HEADER"}
}

// Footer is a <footer> HTML tag
//   Represents a footer for its nearest sectioning content or sectioning root
//   element
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/footer
func Footer() *mogowk.Tag {
	return &mogowk.Tag{Name: "FOOTER"}
}

// H1 is a <h1> HTML tag
//   Represent six levels of section headings
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements
func H1() *mogowk.Tag {
	return &mogowk.Tag{Name: "H1"}
}

// H2 is a <h2> HTML tag
//   Represent six levels of section headings
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements
func H2() *mogowk.Tag {
	return &mogowk.Tag{Name: "H2"}
}

// H3 is a <h3> HTML tag
//   Represent six levels of section headings
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements
func H3() *mogowk.Tag {
	return &mogowk.Tag{Name: "H3"}
}

// H4 is a <h4> HTML tag
//   Represent six levels of section headings
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements
func H4() *mogowk.Tag {
	return &mogowk.Tag{Name: "H4"}
}

// H5 is a <h5> HTML tag
//   Represent six levels of section headings
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements
func H5() *mogowk.Tag {
	return &mogowk.Tag{Name: "H5"}
}

// H6 is a <h6> HTML tag
//   Represent six levels of section headings
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements
func H6() *mogowk.Tag {
	return &mogowk.Tag{Name: "H6"}
}

// HGroup is a <hgroup> HTML tag
//   represents a multi-level heading for a section of a document. It groups a set
//   of <h1>–<h6> elements
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/hgroup
func HGroup() *mogowk.Tag {
	return &mogowk.Tag{Name: "HGROUP"}
}

// Main is a <main> HTML tag
//   Represents the dominant content of the <body> of a document
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/main
func Main() *mogowk.Tag {
	return &mogowk.Tag{Name: "MAIN"}
}

// Nav is a <nav> HTML tag
//   Represents navigation links, either within the current document or to other
//   documents
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/nav
func Nav() *mogowk.Tag {
	return &mogowk.Tag{Name: "NAV"}
}

// Section is a <section> HTML tag
//   Represents a standalone section — which doesn't have a more specific semantic
//   element to represent it
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/section
func Section() *mogowk.Tag {
	return &mogowk.Tag{Name: "SECTION"}
}

// Blockquote is a <blockquote> HTML tag
//   Indicates that the enclosed text is an extended quotation
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/blockquote
func Blockquote() *mogowk.Tag {
	return &mogowk.Tag{Name: "BLOCKQUOTE"}
}

// DD is a <dd> HTML tag
//   Provides the description, definition, or value for the preceding term (<dt>) in
//   a description list (<dl>)
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dd
func DD() *mogowk.Tag {
	return &mogowk.Tag{Name: "DD"}
}

// Div is a <div> HTML tag
//   Is the generic container for flow content. It has no effect on the content or
//   layout until styled using CSS
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/div
func Div() *mogowk.Tag {
	return &mogowk.Tag{Name: "DIV"}
}

// DL is a <dl> HTML tag
//   Represents a description list. The element encloses a list of groups of terms
//   (specified using the <dt> element) and descriptions (provided by <dd> elements)
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dl
func DL() *mogowk.Tag {
	return &mogowk.Tag{Name: "DL"}
}

// DT is a <dt> HTML tag
//   Specifies a term in a description or definition list, and as such must be used
//   inside a <dl> elemen.
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dt
func DT() *mogowk.Tag {
	return &mogowk.Tag{Name: "DT"}
}

// Figcaption is a <figcaption> HTML tag
//   Represents a caption or legend describing the rest of the contents of its
//   parent <figure> element
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/figcaption
func Figcaption() *mogowk.Tag {
	return &mogowk.Tag{Name: "FIGCAPTION"}
}

// Figure is a <figure> HTML tag
//   Represents self-contained content, potentially with an optional caption, which
//   is specified using the (<figcaption>) element
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/figure
func Figure() *mogowk.Tag {
	return &mogowk.Tag{Name: "FIGURE"}
}

// HR is a <hr> HTML tag
//   Represents a thematic break between paragraph-level elements
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/hr
func HR() *mogowk.Tag {
	return &mogowk.Tag{Name: "HR"}
}

// P is a <p> HTML tag
//   Represents a paragraph
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/p
func P() *mogowk.Tag {
	return &mogowk.Tag{Name: "P"}
}

// Pre is a <pre> HTML tag
//   Represents preformatted text which is to be presented exactly as written in the
//   HTML file
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/pre
func Pre() *mogowk.Tag {
	return &mogowk.Tag{Name: "PRE"}
}

// LI is a <li> HTML tag
//   Used to represent an item in a list
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/li
func LI() *mogowk.Tag {
	return &mogowk.Tag{Name: "LI"}
}

// OL is a <ol> HTML tag
//   Represents an ordered list of items — typically rendered as a numbered list
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ol
func OL() *mogowk.Tag {
	return &mogowk.Tag{Name: "OL"}
}

// UL is a <ul> HTML tag
//   Represents an unordered list of items, typically rendered as a bulleted list
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ul
func UL() *mogowk.Tag {
	return &mogowk.Tag{Name: "UL"}
}

// A is a <a> HTML tag
//   With its href attribute, creates a hyperlink to web pages, files or anything
//   else a URL can address
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/a
func A() *mogowk.Tag {
	return &mogowk.Tag{Name: "A"}
}

// Abbr is a <abbr> HTML tag
//   Represents an abbreviation or acronym;
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/abbr
func Abbr() *mogowk.Tag {
	return &mogowk.Tag{Name: "ABBR"}
}

// B is a <b> HTML tag
//   Is used to draw the reader's attention to the element's contents
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/b
func B() *mogowk.Tag {
	return &mogowk.Tag{Name: "B"}
}

// BDI is a <bdi> HTML tag
//   Tells the browser's bidirectional algorithm to treat the text it contains in
//   isolation from its surrounding text
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/bdi
func BDI() *mogowk.Tag {
	return &mogowk.Tag{Name: "BDI"}
}

// BDO is a <bdo> HTML tag
//   Overrides the current directionality of text, so that the text within is
//   rendered in a different direction
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/bdo
func BDO() *mogowk.Tag {
	return &mogowk.Tag{Name: "BDO"}
}

// BR is a <br> HTML tag
//   Produces a line break in text (carriage-return)
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/br
func BR() *mogowk.Tag {
	return &mogowk.Tag{Name: "BR"}
}

// Cite is a <cite> HTML tag
//   Is used to describe a reference to a cited creative work, and must include the
//   title of that work
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/cite
func Cite() *mogowk.Tag {
	return &mogowk.Tag{Name: "CITE"}
}

// Code is a <code> HTML tag
//   Displays its contents styled in a fashion intended to indicate that the text is
//   a short fragment of computer code
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/code
func Code() *mogowk.Tag {
	return &mogowk.Tag{Name: "CODE"}
}

// Data is a <data> HTML tag
//   Links a given content with a machine-readable translation
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/data
func Data() *mogowk.Tag {
	return &mogowk.Tag{Name: "DATA"}
}

// DFN is a <dfn> HTML tag
//   Is used to indicate the term being defined within the context of a definition
//   phrase or sentence
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dfn
func DFN() *mogowk.Tag {
	return &mogowk.Tag{Name: "DFN"}
}

// EM is a <em> HTML tag
//   Marks text that has stress emphasis. The <em> element can be nested, with each
//   level of nesting indicating a greater degree of emphasis
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/em
func EM() *mogowk.Tag {
	return &mogowk.Tag{Name: "EM"}
}

// I is a <i> HTML tag
//   Represents a range of text that is set off from the normal text for some reason
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/i
func I() *mogowk.Tag {
	return &mogowk.Tag{Name: "I"}
}

// KBD is a <kbd> HTML tag
//   Represents a span of inline text denoting textual user input from a keyboard,
//   voice input, or any other text entry device.
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/kbd
func KBD() *mogowk.Tag {
	return &mogowk.Tag{Name: "KBD"}
}

// Mark is a <mark> HTML tag
//   Represents text which is marked or highlighted for reference or notation
//   purposes
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/mark
func Mark() *mogowk.Tag {
	return &mogowk.Tag{Name: "MARK"}
}

// Q is a <q> HTML tag
//   Indicates that the enclosed text is a short inline quotation. Most modern
//   browsers implement this by surrounding the text in quotation marks
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/q
func Q() *mogowk.Tag {
	return &mogowk.Tag{Name: "Q"}
}

// RB is a <rb> HTML tag
//   Is used to delimit the base text component of a <ruby> annotation, i.e. the
//   text that is being annotated.
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/rb
func RB() *mogowk.Tag {
	return &mogowk.Tag{Name: "RB"}
}

// RP is a <rp> HTML tag
//   Is used to provide fall-back parentheses for browsers that do not support
//   display of ruby annotations using the <ruby> element
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/rp
func RP() *mogowk.Tag {
	return &mogowk.Tag{Name: "RP"}
}

// RT is a <rt> HTML tag
//   Specifies the ruby text component of a ruby annotation, which is used to
//   provide pronunciation, translation for East Asian typography.
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/rt
func RT() *mogowk.Tag {
	return &mogowk.Tag{Name: "RT"}
}

// RTC is a <rtc> HTML tag
//   Embraces semantic annotations of characters presented in a ruby of <rb>
//   elements used inside of <ruby> element
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/rtc
func RTC() *mogowk.Tag {
	return &mogowk.Tag{Name: "RTC"}
}

// Ruby is a <ruby> HTML tag
//   Represents a ruby annotation. Ruby annotations are for showing pronunciation of
//   East Asian characters
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ruby
func Ruby() *mogowk.Tag {
	return &mogowk.Tag{Name: "RUBY"}
}

// S is a <s> HTML tag
//   Renders text with a strikethrough, or a line through it. Use the <s> element to
//   represent things that are no longer relevant or no longer accurate
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/s
func S() *mogowk.Tag {
	return &mogowk.Tag{Name: "S"}
}

// Samp is a <samp> HTML tag
//   Is used to enclose inline text which represents sample (or quoted) output from
//   a computer program
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/samp
func Samp() *mogowk.Tag {
	return &mogowk.Tag{Name: "SAMP"}
}

// Small is a <small> HTML tag
//   Represents side-comments and small print, like copyright and legal text,
//   independent of its styled presentation
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/small
func Small() *mogowk.Tag {
	return &mogowk.Tag{Name: "SMALL"}
}

// Span is a <span> HTML tag
//   Is a generic inline container for phrasing content, which does not inherently
//   represent anything
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/span
func Span() *mogowk.Tag {
	return &mogowk.Tag{Name: "SPAN"}
}

// Strong is a <strong> HTML tag
//   Indicates that its contents have strong importance, seriousness, or urgency
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/strong
func Strong() *mogowk.Tag {
	return &mogowk.Tag{Name: "STRONG"}
}

// Sub is a <sub> HTML tag
//   Specifies inline text which should be displayed as subscript for solely
//   typographical reasons
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/sub
func Sub() *mogowk.Tag {
	return &mogowk.Tag{Name: "SUB"}
}

// Sup is a <sup> HTML tag
//   Specifies inline text which is to be displayed as superscript for solely
//   typographical reasons
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/sup
func Sup() *mogowk.Tag {
	return &mogowk.Tag{Name: "SUP"}
}

// Time is a <time> HTML tag
//   Represents a specific period in time
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/time
func Time() *mogowk.Tag {
	return &mogowk.Tag{Name: "TIME"}
}

// U is a <u> HTML tag
//   Represents a span of inline text which should be rendered in a way that
//   indicates that it has a non-textual annotation
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/u
func U() *mogowk.Tag {
	return &mogowk.Tag{Name: "U"}
}

// Var is a <var> HTML tag
//   Represents the name of a variable in a mathematical expression or a programming
//   context
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/var
func Var() *mogowk.Tag {
	return &mogowk.Tag{Name: "VAR"}
}

// WBR is a <wbr> HTML tag
//   Represents a word break opportunity—a position within text where the browser
//   may optionally break a line
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/wbr
func WBR() *mogowk.Tag {
	return &mogowk.Tag{Name: "WBR"}
}

// Area is a <area> HTML tag
//   Defines a hot-spot region on an image, and optionally associates it with a
//   hypertext link
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/area
func Area() *mogowk.Tag {
	return &mogowk.Tag{Name: "AREA"}
}

// Audio is a <audio> HTML tag
//   Is used to embed sound content in documents. It may contain one or more audio
//   sources
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/audio
func Audio() *mogowk.Tag {
	return &mogowk.Tag{Name: "AUDIO"}
}

// Img is a <img> HTML tag
//   Embeds an image into the document
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/img
func Img() *mogowk.Tag {
	return &mogowk.Tag{Name: "IMG"}
}

// Map is a <map> HTML tag
//   Is used with <area> elements to define an image map (a clickable link area)
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/map
func Map() *mogowk.Tag {
	return &mogowk.Tag{Name: "MAP"}
}

// Track is a <track> HTML tag
//   Is used as a child of the media elements <audio> and <video>. It lets you
//   specify timed text tracks (or time-based data)
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/track
func Track() *mogowk.Tag {
	return &mogowk.Tag{Name: "TRACK"}
}

// Video is a <video> HTML tag
//   Embeds a media player which supports video playback into the document
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/video
func Video() *mogowk.Tag {
	return &mogowk.Tag{Name: "VIDEO"}
}

// SVG is a <svg> HTML tag
//   SVG elements can be modified using attributes that specify details about
//   exactly how the element should be handled or rendered
//
// https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute
func SVG() *mogowk.Tag {
	return &mogowk.Tag{Name: "SVG"}
}

// Embed is a <embed> HTML tag
//   Embeds external content at the specified point in the document
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/embed
func Embed() *mogowk.Tag {
	return &mogowk.Tag{Name: "EMBED"}
}

// Iframe is a <iframe> HTML tag
//   Represents a nested browsing context, embedding another HTML page into the
//   current one.
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/iframe
func Iframe() *mogowk.Tag {
	return &mogowk.Tag{Name: "IFRAME"}
}

// Object is a <object> HTML tag
//   Represents an external resource, which can be treated as an image, a nested
//   browsing context, or a resource to be handled by a plugin
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/object
func Object() *mogowk.Tag {
	return &mogowk.Tag{Name: "OBJECT"}
}

// Param is a <param> HTML tag
//   Defines parameters for an <object> element
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/param
func Param() *mogowk.Tag {
	return &mogowk.Tag{Name: "PARAM"}
}

// Picture is a <picture> HTML tag
//   Contains zero or more <source> elements and one <img> element to offer
//   alternative versions of an image for different display scenarios
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/picture
func Picture() *mogowk.Tag {
	return &mogowk.Tag{Name: "PICTURE"}
}

// Source is a <source> HTML tag
//   Specifies multiple media resources for the <picture>, the <audio>, or the
//   <video> element
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/source
func Source() *mogowk.Tag {
	return &mogowk.Tag{Name: "SOURCE"}
}

// Canvas is a <canvas> HTML tag
//   With either the canvas scripting API or the WebGL API to draw graphics and
//   animations
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/canvas
func Canvas() *mogowk.Tag {
	return &mogowk.Tag{Name: "CANVAS"}
}

// Script is a <script> HTML tag
//   Defines a section of HTML to be inserted if a script type on the page is
//   unsupported or if scripting is currently turned off in the browser
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/script
func Script() *mogowk.Tag {
	return &mogowk.Tag{Name: "SCRIPT"}
}

// NoScript is a <noscript> HTML tag
//   Is used to embed or reference executable code; this is typically used to embed
//   or refer to JavaScript code
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/noscript
func NoScript() *mogowk.Tag {
	return &mogowk.Tag{Name: "NOSCRIPT"}
}

// Del is a <del> HTML tag
//   Represents a range of text that has been deleted from a document
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/del
func Del() *mogowk.Tag {
	return &mogowk.Tag{Name: "DEL"}
}

// Ins is a <ins> HTML tag
//   Represents a range of text that has been added to a document
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ins
func Ins() *mogowk.Tag {
	return &mogowk.Tag{Name: "INS"}
}

// Table is a <table> HTML tag
//   Represents tabular data — that is, information presented in a two-dimensional
//   table comprised of rows and columns of cells containing data
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/table
func Table() *mogowk.Tag {
	return &mogowk.Tag{Name: "TABLE"}
}

// Caption is a <caption> HTML tag
//   Specifies the caption (or title) of a table
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/caption
func Caption() *mogowk.Tag {
	return &mogowk.Tag{Name: "CAPTION"}
}

// THead is a <thead> HTML tag
//   Defines a set of rows defining the head of the columns of the table
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/thead
func THead() *mogowk.Tag {
	return &mogowk.Tag{Name: "THEAD"}
}

// TBody is a <tbody> HTML tag
//   Encapsulates a set of table rows (<tr>), indicating that they comprise the body
//   of the table
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/tbody
func TBody() *mogowk.Tag {
	return &mogowk.Tag{Name: "TBODY"}
}

// TFoot is a <tfoot> HTML tag
//   Defines a set of rows summarizing the columns of the table
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/tfoot
func TFoot() *mogowk.Tag {
	return &mogowk.Tag{Name: "TFOOT"}
}

// TH is a <th> HTML tag
//   Defines a cell as header of a group of table cells
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/th
func TH() *mogowk.Tag {
	return &mogowk.Tag{Name: "TH"}
}

// TR is a <tr> HTML tag
//   Defines a row of cells in a table
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/tr
func TR() *mogowk.Tag {
	return &mogowk.Tag{Name: "TR"}
}

// TD is a <td> HTML tag
//   Defines a cell of a table that contains data. It participates in the table
//   model
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/td
func TD() *mogowk.Tag {
	return &mogowk.Tag{Name: "TD"}
}

// Col is a <col> HTML tag
//   Defines a column within a table and is used for defining common semantics on
//   all common cells
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/col
func Col() *mogowk.Tag {
	return &mogowk.Tag{Name: "COL"}
}

// ColGroup is a <colgroup> HTML tag
//   Defines a group of columns within a table
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/colgroup
func ColGroup() *mogowk.Tag {
	return &mogowk.Tag{Name: "COLGROUP"}
}

// Button is a <button> HTML tag
//   Represents a clickable button, used to submit forms or anywhere in a document
//   for accessible, standard button functionality
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/button
func Button() *mogowk.Tag {
	return &mogowk.Tag{Name: "BUTTON"}
}

// DataList is a <datalist> HTML tag
//   Contains a set of <option> elements that represent the permissible
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/datalist
func DataList() *mogowk.Tag {
	return &mogowk.Tag{Name: "DATALIST"}
}

// Fieldset is a <fieldset> HTML tag
//   Is used to group several controls as well as labels (<label>) within a web form
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/fieldset
func Fieldset() *mogowk.Tag {
	return &mogowk.Tag{Name: "FIELDSET"}
}

// Form is a <form> HTML tag
//   Represents a document section containing interactive controls for submitting
//   information
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/form
func Form() *mogowk.Tag {
	return &mogowk.Tag{Name: "FORM"}
}

// Input is a <input> HTML tag
//   Is used to create interactive controls for web-based forms in order to accept
//   data from the user
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input
func Input() *mogowk.Tag {
	return &mogowk.Tag{Name: "INPUT"}
}

// Label is a <label> HTML tag
//   Represents a caption for an item in a user interface
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/label
func Label() *mogowk.Tag {
	return &mogowk.Tag{Name: "LABEL"}
}

// Legend is a <legend> HTML tag
//   Represents a caption for the content of its parent <fieldset>
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/legend
func Legend() *mogowk.Tag {
	return &mogowk.Tag{Name: "LEGEND"}
}

// Meter is a <meter> HTML tag
//   Represents either a scalar value within a known range or a fractional value
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meter
func Meter() *mogowk.Tag {
	return &mogowk.Tag{Name: "METER"}
}

// Output is a <output> HTML tag
//   Is a container element into which a site or app can inject the results of a
//   calculation or the outcome of a user action
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/output
func Output() *mogowk.Tag {
	return &mogowk.Tag{Name: "OUTPUT"}
}

// Progress is a <progress> HTML tag
//   Displays an indicator showing the completion progress of a task, typically
//   displayed as a progress bar
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/progress
func Progress() *mogowk.Tag {
	return &mogowk.Tag{Name: "PROGRESS"}
}

// Select is a <select> HTML tag
//   Represents a control that provides a menu of options
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/select
func Select() *mogowk.Tag {
	return &mogowk.Tag{Name: "SELECT"}
}

// OptGroup is a <optgroup> HTML tag
//   Creates a grouping of options within a <select> element
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/optgroup
func OptGroup() *mogowk.Tag {
	return &mogowk.Tag{Name: "OPTGROUP"}
}

// Option is a <option> HTML tag
//   Is used to define an item contained in a <select>, an <optgroup>, or a
//   <datalist> element
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/option
func Option() *mogowk.Tag {
	return &mogowk.Tag{Name: "OPTION"}
}

// TextArea is a <textarea> HTML tag
//   Represents a multi-line plain-text editing control, useful when you want to
//   allow users to enter a sizeable amount of free-form text
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/textarea
func TextArea() *mogowk.Tag {
	return &mogowk.Tag{Name: "TEXTAREA"}
}

// Details is a <details> HTML tag
//   Creates a disclosure widget in which information is visible only when the
//   widget is toggled into an "open" state
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/details
func Details() *mogowk.Tag {
	return &mogowk.Tag{Name: "DETAILS"}
}

// Summary is a <summary> HTML tag
//   Specifies a summary, caption, or legend for a <details> element's disclosure
//   box
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/summary
func Summary() *mogowk.Tag {
	return &mogowk.Tag{Name: "SUMMARY"}
}

// Dialog is a <dialog> HTML tag
//   represents a dialog box or other interactive component, such as a dismissable
//   alert, inspector, or subwindow
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dialog
func Dialog() *mogowk.Tag {
	return &mogowk.Tag{Name: "DIALOG"}
}
