//go:build js && wasm

package mogowk

import (
	"reflect"
	"syscall/js"
)

// Tag represents HTML tag with children.
// Call CompileElement if you want to use tag in your component.
// Use With... method to fill tag's fields.
type Tag struct {
	Name       string                 // Name of the HTML tag.
	Classes    []string               // Values list of the "class" attribute.
	Events     []*EventListener       // Event Listeners of the tag.
	Attributes map[string]interface{} // Set of the HTML tag's attributes.

	Children []Renderer // List of the user Components or other Tags.

	Text       string // Value of the Text Node.
	IsTextNode bool   // If set to true then Tag will be rendered as a TextNode without classes and attrs.

	isPreCompiled bool
	preCompiled   js.Value
}

// Render implements Renderer interface not for users.
func (t *Tag) Render() *Tag { return t }

// getComponent implements Renderer interface returns nil
// as a signal that it is not a component.
func (*Tag) getComponent() *Component { return nil }

// compileElement compiles tag to a js element.
func (t *Tag) compileElement() js.Value {
	if t.isPreCompiled {
		return t.preCompiled
	}

	if t.IsTextNode {
		return document.Call("createTextNode", t.Text)
	}

	el := document.Call("createElement", t.Name)

	classList := el.Get("classList")
	for _, class := range t.Classes {
		classList.Call("add", class)
	}

	for name, value := range t.Attributes {
		el.Call("setAttribute", name, value)
	}

	for _, event := range t.Events {
		SetEventListenerInto(el, event)
	}

	return el
}

// CompileElement compiles Tag to a js element that allows you
// to use it in the your component.
func (t *Tag) CompileElement() js.Value {
	t.preCompiled = t.compileElement()
	t.isPreCompiled = true
	return t.preCompiled
}

// WithClasses adds list of classes into Tag.
func (t *Tag) WithClasses(classes ...string) *Tag {
	if t.Classes == nil {
		t.Classes = make([]string, 0, len(classes))
	}

	t.Classes = append(t.Classes, classes...)
	return t
}

// WithChildren adds other Tags or user components into Tag.
func (t *Tag) WithChildren(tagsOrComponents ...Renderer) *Tag {
	if t.Children == nil {
		t.Children = make([]Renderer, 0, len(tagsOrComponents))
	}

	t.Children = append(t.Children, tagsOrComponents...)
	return t
}

// WithListChild adds list of the Tags or user components into Tag
// Uses reflection and may be slower than manual converting
// You can convert your list to Renderers list and use WithChildren:
//
// renderers := make([]mogowk.Renderer, len(yourList))
// for i, item := range yourList {
//   renderers[i] = item
// }
// mogowk.Div().WithChildren(renderers...)
func (t *Tag) WithListChild(list interface{}) *Tag {
	l := reflect.ValueOf(list)
	if l.Kind() != reflect.Slice {
		panic("mogowk: WithListChild: given a non-slice type")
	}

	renderers := make([]Renderer, l.Len())
	for i := 0; i < l.Len(); i++ {
		r, ok := l.Index(i).Interface().(Renderer)
		if !ok {
			panic("mogowk: WithListChild: given a not Renderer type in list")
		}
		renderers[i] = r
	}

	return t.WithChildren(renderers...)
}

// WithEvents adds list of EventListeners into Tag.
func (t *Tag) WithEvents(events ...*EventListener) *Tag {
	if t.Events == nil {
		t.Events = make([]*EventListener, 0, len(events))
	}

	t.Events = append(t.Events, events...)
	return t
}

// WithAttribute adds HTML tag's attribute into Tag.
func (t *Tag) WithAttribute(name string, value interface{}) *Tag {
	if t.Attributes == nil {
		t.Attributes = make(map[string]interface{}, 2)
	}

	t.Attributes[name] = value
	return t
}

// WithValue sugar method adds value attribute to Tag.
func (t *Tag) WithValue(value interface{}) *Tag {
	return t.WithAttribute("value", value)
}
