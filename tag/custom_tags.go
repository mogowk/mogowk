//go:build js && wasm

package tag

import (
	"gitlab.com/mogowk/mogowk"
)

// Text creates a new Text node like a document.createTextNode() method
//
// https://developer.mozilla.org/en-US/docs/Web/API/Document/createTextNode
func Text(text string) *mogowk.Tag {
	return &mogowk.Tag{IsTextNode: true, Text: text}
}
