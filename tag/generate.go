// +build ignore

package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"
	"text/template"
)

var (
	funcMap = template.FuncMap{
		"Title":    strings.Title,
		"ToLower":  strings.ToLower,
		"ToUpper":  strings.ToUpper,
		"Wordwrap": wordwrap,
	}

	fileName = "tag.gen.go"
)

// https://github.com/eidolon/wordwrap
func wordwrap(input string) string {
	var wrapped string
	// Split string into array of words
	words := strings.Fields(input)

	if len(words) == 0 {
		return wrapped
	}
	limit := 80
	remaining := limit

	for _, word := range words {
		if len(word)+1 > remaining {
			if len(wrapped) > 0 {
				wrapped += "\n//   "
			}
			wrapped += word
			remaining = limit - len(word)
		} else {
			if len(wrapped) > 0 {
				wrapped += " "
			}
			wrapped += word
			remaining = remaining - (len(word) + 1)
		}
	}
	return wrapped
}

type tagTmplData struct {
	TagName     string
	FuncName    string
	Description string
	Link        string
}

func main() {
	tagTmpl, err := template.New("tag").Funcs(funcMap).Parse(`
// {{ .FuncName }} is a <{{ .TagName | ToLower }}> HTML tag
//   {{ .Description | Wordwrap }}
//
// {{ .Link }}
func {{ .FuncName }}() *mogowk.Tag {
	return &mogowk.Tag{Name: "{{ .TagName | ToUpper }}"}
}
`)

	if err != nil {
		log.Fatalf("failed to Parse template: %s", err)
	}

	file, err := os.Create(fileName)
	if err != nil {
		log.Fatalf("failed to create file: %s", err)
	}
	defer file.Close()

	_, err = fmt.Fprintln(file, `//go:build js && wasm
// Package tag contains constructors of HTML Tags.
// Generates with texts from https://developer.mozilla.org/en-US/docs/Web/HTML/Element page
package tag

import (
	"gitlab.com/mogowk/mogowk"
)
`)

	if err != nil {
		log.Fatalf("failed to print file head: %s", err)
	}

	for _, tag := range tags {
		if tag.Link == "" {
			tag.Link = "https://developer.mozilla.org/en-US/docs/Web/HTML/Element/" + tag.TagName
		}

		err = tagTmpl.Execute(file, tag)
		if err != nil {
			log.Fatalf("failed to Execute template: %s", err)
		}
	}

	err = exec.Command("go", "fmt", fileName).Run()
	if err != nil {
		log.Fatalf("failed to fmt file: %s", err)
	}

	log.Printf("Success generated %s\n", fileName)
}

var tags = []tagTmplData{
	// Sectioning root
	{"body", "Body", "Defines the document's body", ""},

	// Document metadata
	{"link", "Link", "Specifies relationships between the current document and an external resource", ""},
	{"style", "Style", "Contains style information for a document, or part of a document", ""},

	// Content sectioning
	{"address", "Address", "Indicates that the enclosed HTML provides contact information", ""},
	{"article", "Article", "Represents a self-contained composition in a document, page, etc...", ""},
	{"aside", "Aside", "Represents a portion of a document whose content is only indirectly related to the document's main content", ""},
	{"header", "Header", "Represents introductory content, typically a group of introductory or navigational aids", ""},
	{"footer", "Footer", "Represents a footer for its nearest sectioning content or sectioning root element", ""},
	{"h1", "H1", "Represent six levels of section headings", "https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements"},
	{"h2", "H2", "Represent six levels of section headings", "https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements"},
	{"h3", "H3", "Represent six levels of section headings", "https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements"},
	{"h4", "H4", "Represent six levels of section headings", "https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements"},
	{"h5", "H5", "Represent six levels of section headings", "https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements"},
	{"h6", "H6", "Represent six levels of section headings", "https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements"},
	{"hgroup", "HGroup", "represents a multi-level heading for a section of a document. It groups a set of <h1>–<h6> elements", ""},
	{"main", "Main", "Represents the dominant content of the <body> of a document", ""},
	{"nav", "Nav", "Represents navigation links, either within the current document or to other documents", ""},
	{"section", "Section", "Represents a standalone section — which doesn't have a more specific semantic element to represent it", ""},

	// Text content
	{"blockquote", "Blockquote", "Indicates that the enclosed text is an extended quotation", ""},
	{"dd", "DD", "Provides the description, definition, or value for the preceding term (<dt>) in a description list (<dl>)", ""},
	{"div", "Div", "Is the generic container for flow content. It has no effect on the content or layout until styled using CSS", ""},
	{"dl", "DL", "Represents a description list. The element encloses a list of groups of terms (specified using the <dt> element) and descriptions (provided by <dd> elements)", ""},
	{"dt", "DT", "Specifies a term in a description or definition list, and as such must be used inside a <dl> elemen.", ""},
	{"figcaption", "Figcaption", "Represents a caption or legend describing the rest of the contents of its parent <figure> element", ""},
	{"figure", "Figure", "Represents self-contained content, potentially with an optional caption, which is specified using the (<figcaption>) element", ""},
	{"hr", "HR", "Represents a thematic break between paragraph-level elements", ""},
	{"p", "P", "Represents a paragraph", ""},
	{"pre", "Pre", "Represents preformatted text which is to be presented exactly as written in the HTML file", ""},
	// Lists
	{"li", "LI", "Used to represent an item in a list", ""},
	{"ol", "OL", "Represents an ordered list of items — typically rendered as a numbered list", ""},
	{"ul", "UL", "Represents an unordered list of items, typically rendered as a bulleted list", ""},

	// Inline text semantics
	{"a", "A", "With its href attribute, creates a hyperlink to web pages, files or anything else a URL can address", ""},
	{"abbr", "Abbr", "Represents an abbreviation or acronym;", ""},
	{"b", "B", "Is used to draw the reader's attention to the element's contents", ""},
	{"bdi", "BDI", "Tells the browser's bidirectional algorithm to treat the text it contains in isolation from its surrounding text", ""},
	{"bdo", "BDO", "Overrides the current directionality of text, so that the text within is rendered in a different direction", ""},
	{"br", "BR", "Produces a line break in text (carriage-return)", ""},
	{"cite", "Cite", "Is used to describe a reference to a cited creative work, and must include the title of that work", ""},
	{"code", "Code", "Displays its contents styled in a fashion intended to indicate that the text is a short fragment of computer code", ""},
	{"data", "Data", "Links a given content with a machine-readable translation", ""},
	{"dfn", "DFN", "Is used to indicate the term being defined within the context of a definition phrase or sentence", ""},
	{"em", "EM", "Marks text that has stress emphasis. The <em> element can be nested, with each level of nesting indicating a greater degree of emphasis", ""},
	{"i", "I", "Represents a range of text that is set off from the normal text for some reason", ""},
	{"kbd", "KBD", "Represents a span of inline text denoting textual user input from a keyboard, voice input, or any other text entry device.", ""},
	{"mark", "Mark", "Represents text which is marked or highlighted for reference or notation purposes", ""},
	{"q", "Q", "Indicates that the enclosed text is a short inline quotation. Most modern browsers implement this by surrounding the text in quotation marks", ""},
	{"rb", "RB", "Is used to delimit the base text component of a  <ruby> annotation, i.e. the text that is being annotated.", ""},
	{"rp", "RP", "Is used to provide fall-back parentheses for browsers that do not support display of ruby annotations using the <ruby> element", ""},
	{"rt", "RT", "Specifies the ruby text component of a ruby annotation, which is used to provide pronunciation, translation for East Asian typography.", ""},
	{"rtc", "RTC", "Embraces semantic annotations of characters presented in a ruby of <rb> elements used inside of <ruby> element", ""},
	{"ruby", "Ruby", "Represents a ruby annotation. Ruby annotations are for showing pronunciation of East Asian characters", ""},
	{"s", "S", "Renders text with a strikethrough, or a line through it. Use the <s> element to represent things that are no longer relevant or no longer accurate", ""},
	{"samp", "Samp", "Is used to enclose inline text which represents sample (or quoted) output from a computer program", ""},
	{"small", "Small", "Represents side-comments and small print, like copyright and legal text, independent of its styled presentation", ""},
	{"span", "Span", "Is a generic inline container for phrasing content, which does not inherently represent anything", ""},
	{"strong", "Strong", "Indicates that its contents have strong importance, seriousness, or urgency", ""},
	{"sub", "Sub", "Specifies inline text which should be displayed as subscript for solely typographical reasons", ""},
	{"sup", "Sup", "Specifies inline text which is to be displayed as superscript for solely typographical reasons", ""},
	{"time", "Time", "Represents a specific period in time", ""},
	{"u", "U", "Represents a span of inline text which should be rendered in a way that indicates that it has a non-textual annotation", ""},
	{"var", "Var", "Represents the name of a variable in a mathematical expression or a programming context", ""},
	{"wbr", "WBR", "Represents a word break opportunity—a position within text where the browser may optionally break a line", ""},

	// Image and multimedia
	{"area", "Area", "Defines a hot-spot region on an image, and optionally associates it with a hypertext link", ""},
	{"audio", "Audio", "Is used to embed sound content in documents. It may contain one or more audio sources", ""},
	{"img", "Img", "Embeds an image into the document", ""},
	{"map", "Map", "Is used with <area> elements to define an image map (a clickable link area)", ""},
	{"track", "Track", "Is used as a child of the media elements <audio> and <video>. It lets you specify timed text tracks (or time-based data)", ""},
	{"video", "Video", "Embeds a media player which supports video playback into the document", ""},
	{"svg", "SVG", "SVG elements can be modified using attributes that specify details about exactly how the element should be handled or rendered", "https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute"},

	// Embedded content
	{"embed", "Embed", "Embeds external content at the specified point in the document", ""},
	{"iframe", "Iframe", "Represents a nested browsing context, embedding another HTML page into the current one.", ""},
	{"object", "Object", "Represents an external resource, which can be treated as an image, a nested browsing context, or a resource to be handled by a plugin", ""},
	{"param", "Param", "Defines parameters for an <object> element", ""},
	{"picture", "Picture", "Contains zero or more <source> elements and one <img> element to offer alternative versions of an image for different display scenarios", ""},
	{"source", "Source", "Specifies multiple media resources for the <picture>, the <audio>, or the <video> element", ""},

	// Scripting
	{"canvas", "Canvas", "With either the canvas scripting API or the WebGL API to draw graphics and animations", ""},
	{"script", "Script", "Defines a section of HTML to be inserted if a script type on the page is unsupported or if scripting is currently turned off in the browser", ""},
	{"noscript", "NoScript", "Is used to embed or reference executable code; this is typically used to embed or refer to JavaScript code", ""},

	// Demarcating edits
	{"del", "Del", "Represents a range of text that has been deleted from a document", ""},
	{"ins", "Ins", "Represents a range of text that has been added to a document", ""},

	// Table content
	{"table", "Table", "Represents tabular data — that is, information presented in a two-dimensional table comprised of rows and columns of cells containing data", ""},
	{"caption", "Caption", "Specifies the caption (or title) of a table", ""},
	{"thead", "THead", "Defines a set of rows defining the head of the columns of the table", ""},
	{"tbody", "TBody", "Encapsulates a set of table rows (<tr>), indicating that they comprise the body of the table", ""},
	{"tfoot", "TFoot", "Defines a set of rows summarizing the columns of the table", ""},
	{"th", "TH", "Defines a cell as header of a group of table cells", ""},
	{"tr", "TR", "Defines a row of cells in a table", ""},
	{"td", "TD", "Defines a cell of a table that contains data. It participates in the table model", ""},
	{"col", "Col", "Defines a column within a table and is used for defining common semantics on all common cells", ""},
	{"colgroup", "ColGroup", "Defines a group of columns within a table", ""},

	// Forms
	{"button", "Button", "Represents a clickable button, used to submit forms or anywhere in a document for accessible, standard button functionality", ""},
	{"datalist", "DataList", "Contains a set of <option> elements that represent the permissible", ""},
	{"fieldset", "Fieldset", "Is used to group several controls as well as labels (<label>) within a web form", ""},
	{"form", "Form", "Represents a document section containing interactive controls for submitting information", ""},
	{"input", "Input", "Is used to create interactive controls for web-based forms in order to accept data from the user", ""},
	{"label", "Label", "Represents a caption for an item in a user interface", ""},
	{"legend", "Legend", "Represents a caption for the content of its parent <fieldset>", ""},
	{"meter", "Meter", "Represents either a scalar value within a known range or a fractional value", ""},
	{"output", "Output", "Is a container element into which a site or app can inject the results of a calculation or the outcome of a user action", ""},
	{"progress", "Progress", "Displays an indicator showing the completion progress of a task, typically displayed as a progress bar", ""},
	{"select", "Select", "Represents a control that provides a menu of options", ""},
	{"optgroup", "OptGroup", "Creates a grouping of options within a <select> element", ""},
	{"option", "Option", "Is used to define an item contained in a <select>, an <optgroup>, or a <datalist> element", ""},
	{"textarea", "TextArea", "Represents a multi-line plain-text editing control, useful when you want to allow users to enter a sizeable amount of free-form text", ""},

	// Interactive elements
	{"details", "Details", `Creates a disclosure widget in which information is visible only when the widget is toggled into an "open" state`, ""},
	{"summary", "Summary", "Specifies a summary, caption, or legend for a <details> element's disclosure box", ""},
	{"dialog", "Dialog", "represents a dialog box or other interactive component, such as a dismissable alert, inspector, or subwindow", ""},
}
