.PHONY: generate lints lints_fix

generate:
	cd ./event/ && go run ./generate.go
	cd ./tag/ && go run ./generate.go

lints:
	GOOS=js GOARCH=wasm golangci-lint run -c ./.linters.yml

lints_fix:
	GOOS=js GOARCH=wasm golangci-lint run --fix -c ./.linters.yml

